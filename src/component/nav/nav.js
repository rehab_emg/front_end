import React, { Component } from 'react';
import { connect } from 'dva';
import { Button, Layout, Menu, Icon, Card, Col, Row } from 'antd';
import styles from './nav.css';
const { Header, Footer, Sider, Content } = Layout;

class nav extends Component {


  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }


  render() {
    return (
      <Layout>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          style={{ height: '750px' }}
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <Icon type="user" />
              <span>nav 1</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="video-camera" />
              <span>nav 2</span>
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="upload" />
              <span>nav 3</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
              style={{ padding: '25px' }}
            />
          </Header>
          <Row>
            <Col span={8}>
              <Card style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 200 }} >
                123123
              </Card>
            </Col>
            <Col span={8}>
              <Card style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 200 }} >
                123123
              </Card>
            </Col>
            <Col span={8}>
              <Card style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 200 }} >
                123123
              </Card>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <Card style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 300 }} >
                123123
              </Card>
            </Col>
            <Col span={16}>
              <Card style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 300 }} >
                123123
              </Card>
            </Col>
          </Row>
        </Layout>
      </Layout>
    );
  }

}

export default connect()(nav);

import React, { Component } from 'react';
import { Row, Col, Drawer, Card, Avatar, Icon } from 'antd';
import CardBasicLineChart from '../card/chart/BasicLineChart';
import apiConfig from '../../config';
import CardBasicAnalysisLineChart from '../../component/card/chart/BasicAnalysisLineChart';
import _ from 'lodash';
import style from './ListDrawer.css';

class ListDrawer extends Component {

  state = {
    pStyle: {
      fontSize: 16,
      color: 'rgba(0,0,0,0.85)',
      lineHeight: '24px',
      display: 'block',
      verticalAlign: 'middle',
      marginBottom: 16,
    },
    // title: '個人化復健環境',
    // content: '藉由收集個人基本的生理參數，經統計及資料分析後，推薦一個',
    // content2: '最適合自己的復健方式，提供使用者參考，並根據使用者過去的',
    // content3: '復健成效來判斷是否該加強復健的課程'
  }

  componentDidMount() {
  }

  onClose = () => {
    const { onClose } = this.props;
    const value = false;
    onClose(value);
  };

  render() {
    const { record } = this.props;
    const { pStyle } = this.state;
    console.log(record);
    return (
      _.isNull(record) ? null :
        <Drawer
          width={1200}
          placement="right"
          closable={false}
          onClose={this.onClose}
          visible={this.props.visible}
        >
          <div>
            <Row>
              <Col className="gutter-row" span={8}>
                {
                  _.isEmpty(record.img) ? <Avatar icon="user" shape="square" size={500} /> : <img alt="pic" style={{ width: '170px', display: 'inlineBlock', 'WebkitBorderRadius': '10px', 'MozBorderRadius': '10px', 'borderRadius': '50%' }} src={`${apiConfig.api}${record.img}`} />
                }

              </Col>
              <Col className="gutter-row" span={16}>
                <div style={{ padding: '50px 0' }}>
                  <p style={{ ...pStyle, marginBottom: 24, fontSize: 25, fontWeight: 600 }}>{record.part}</p>
                  <p style={{ ...pStyle, marginBottom: 24 }}>{record.name}</p>
                  <p style={{ ...pStyle, marginBottom: 24 }}>{record.isLR}</p>
                </div>
              </Col>
            </Row>
            <Row>
              <div style={{ padding: '50px 0' }}>
                <p style={{ marginRight: 8, display: 'inline-block', color: 'rgba(0,0,0,0.85)' }}>
                  各項數據:
                </p>
                <p style={{ ...pStyle, marginBottom: 24 }}>IEMG:{record.iemg}</p>
              </div>
            </Row>

          </div>
          <Row>
            <Col className="gutter-row" span={24}>
              <Card title="sEMG" bordered={false}><CardBasicLineChart recordData={record.data}></CardBasicLineChart></Card>
            </Col>
            <Col lg={8}>
              <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />RMS</div>} bordered={false}><CardBasicAnalysisLineChart container="record_rms" data={record}></CardBasicAnalysisLineChart></Card>
            </Col>
            <Col lg={8}>
              <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />MF</div>} bordered={false}><CardBasicAnalysisLineChart container="record_mf" data={record}></CardBasicAnalysisLineChart></Card>
            </Col>
            <Col lg={8}>
              <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />MPF</div>} bordered={false}><CardBasicAnalysisLineChart container="record_mpf" data={record}></CardBasicAnalysisLineChart></Card>
            </Col>

          </Row>
        </Drawer>
    )
  }
}

export default (ListDrawer);

import React, { Component } from 'react';
import { Form, Row, Icon, Input, Col, Button, DatePicker, Radio, Upload, Modal } from 'antd';
import styles from './Profile.css';
import moment from 'moment';
import apiConfig from '../../config';
import _ from 'lodash';
const FormItem = Form.Item;

class Profile extends Component {

  state = {
    name: '',
    type: '',
    required: '',
    sex: this.props.userData.sex,
    isloading: false,
    fileList: null,
    previewImage: null,
    previewVisible: false,
  }

  handleonChange = (changeEvent) => {
    this.setState({
      sex: changeEvent.target.value,
    })
  }

  handleSubmit = (e) => {
    const { handleEdit } = this.props;
    const { fileList, previewImage } = this.state;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        // const formData = new FormData();
        //  formData.set('name', values.name);
        //  formData.set('cellphone', values.cellphone);
        //  formData.set('email', values.email);
        //  formData.set('buffer', previewImage);
        //  formData.set('fileName', `${fileList.name}`);
        _.isNull(previewImage)? values = { ...values, buffer: previewImage, fileName: '' } :values = { ...values, buffer: previewImage, fileName: `${fileList[0].name}` }
        console.log(values);
        //formData.set('values', JSON.stringify(values));K
        //console.log(formData.get('values'));
        handleEdit(values);
      } else {
        console.log(err);
      }
    });
  }

  getBase64(file) {
    let document = "";
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      document = reader.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };

    return document;
  }

  componentDidMount() {
    this.checkuserimg();
  }

  checkuserimg = () => {
    const { userData } = this.props;
    _.isNull(userData.img) ? console.log('no img') :
      this.setState({
        fileList: [{ uid: '-1', name: 'user.png', status: 'done', thumbUrl: `${apiConfig.api}/${userData.img}` }]
      },() =>{console.log(this.state.fileList)});
  }

  handleSubmitAvatar = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      _.set(file, 'thumbUrl', reader.result);
      this.setState({
        fileList: [file],
        previewImage: file.thumbUrl,
      }, () => console.log(this.state))
    };

    return false;
  }

  handlePreview = (file) => {
    const { fileList } = this.state;
    this.setState({
      previewImage: fileList.thumbUrl,
      previewVisible: true,
    });
  }

  handleCancel = () => {
    this.setState({
      previewImage: null,
      previewVisible: false,
    });
  }

  render() {
    const { fileList, previewVisible, previewImage } = this.state;
    const { userData } = this.props;
    const { getFieldDecorator } = this.props.form;
    const config = {
      rules: [{ type: 'object', required: true, message: '請選擇日期!' }],
      initialValue: moment(userData.birthday, 'YYYY-MM-DD')
    };
    return (
      _.isNull(userData) ? null :
        <Form onSubmit={this.handleSubmit} style={{ minHeight: '1200px' }} >
          <Row>
            <Col  xs={{span:24}} lg={{span:24}} xl={{span:9 }}>
              <div className={styles.containerfluid}>
                <Upload
                  fileList={fileList}
                  className={styles.uploadPreview}
                  beforeUpload={this.handleSubmitAvatar}
                  onPreview={this.handlePreview}
                  onRemove={() => {
                    this.setState({
                      fileList: null,
                      previewImage: null,
                      previewVisible: false,
                    })
                  }}
                  listType="picture-card">
                  {
                    _.isNull(fileList) ? (
                      <div className={styles.uploadPlus}>
                        <div><Icon style={{ display: 'block' }} type='plus' />上傳圖片</div>
                      </div>
                    ) : null
                  }
                </Upload>
                <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                  <img alt="example" style={{ width: '100%' }} src={previewImage} />
                </Modal>
              </div>
            </Col>
            <Col xs={{span:24}} lg={{span:24}} xl={{span:14}} >
              <Row>
                <Col span={24}>
                  <FormItem label="帳號">
                    {getFieldDecorator('account', {
                      rules: [{ required: true, message: '請輸入帳號!' }],
                      initialValue: userData.account
                    })(
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} size="large" placeholder="Account" disabled={true} />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="電子信箱">
                    {getFieldDecorator('email', {
                      rules: [{ type: 'email', message: '您輸入的不是電子信箱!' }, { required: true, message: '請輸入電子信箱' }],
                      initialValue: userData.email
                    })(
                      <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="姓名">
                    {getFieldDecorator('name', {
                      rules: [{ required: true, message: '請輸入姓名!' }],
                      initialValue: userData.name
                    })(
                      <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Name" />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="生日">
                    {getFieldDecorator('birthday', config)(
                      <DatePicker style={{ width: '100%' }} placeholder="Birth" disabled={true} />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="性別">
                    {getFieldDecorator('sex', { rules: [{ required: true, message: '請選擇性別!' }], initialValue: userData.sex })(
                      <Radio.Group style={{ width: '100%', textAlign: 'center' }} onChange={this.handleonChange} disabled={true}>
                        <Radio.Button key={1} value={1} style={{ width: '50%' }} className={this.state.sex === 1 ? `${styles.bluebtn} ${styles.bluebtn_focus}` : styles.bluebtn}>男</Radio.Button>
                        <Radio.Button key={2} value={2} style={{ width: '50%' }} className={this.state.sex === 2 ? `${styles.redbtn} ${styles.redbtn_focus}` : styles.redbtn}>女</Radio.Button>
                      </Radio.Group>
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="電話">
                    {getFieldDecorator('cellphone', {
                      rules: [{ required: true, message: '請輸入電話!' }, { max: 10, message: '請輸入正確的電話號碼' }],
                      initialValue: userData.cellphone
                    })(
                      <Input prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Phone" />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem style={{ 'paddingTop': '20px', margin: '0', textAlign: 'right' }} >
                    <Button loading={this.props.isloading} type="primary" htmlType="submit" className={styles.registerformbutton}>
                      修改確認
                    </Button>
                  </FormItem>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
    )
  }

}

export default Form.create()(Profile);

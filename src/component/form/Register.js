import React, { Component } from 'react';
import { Form, Row, Icon, Input, Checkbox, Button, DatePicker, Radio } from 'antd';
import styles from './Register.css';
import moment from 'moment'
import _ from 'lodash';
const FormItem = Form.Item;

class Register extends Component {

  state = {
    name: '',
    type: '',
    required: '',
    sex: 1,
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('密碼不一致!');
    } else {
      callback();
    }
  }

  disabledDate = (current) => {
    return current && current > moment().endOf('day');
  }

  handleonChange = (changeEvent) => {
    this.setState({
      sex: changeEvent.target.value,
    })
    console.log(this.state.sex);
  }

  handleSubmit = (e) => {
    const { handleSubmit } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const callback = () => {
          this.props.form.resetFields();
        }
        handleSubmit(values, callback);
      } else {
        console.log(err);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const config = {
      rules: [{ type: 'object', required: true, message: '請選擇日期!' }],
      disabledDate: this.disabledDate(),
    };
    return (
      <Form onSubmit={this.handleSubmit} className={styles.loginform} >
        <h2 className={styles.formtitle}><strong>註冊</strong></h2>
        <FormItem label="帳號">
          {getFieldDecorator('account', {
            rules: [{ required: true, message: '請輸入帳號!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Account" />
          )}
        </FormItem>
        <FormItem label="電子信箱">
          {getFieldDecorator('email', {
            rules: [{ type: 'email', message: '您輸入的不是電子信箱!' }, { required: true, message: '請輸入電子信箱', }],
          })(
            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
          )}
        </FormItem>
        <FormItem label="密碼">
          {getFieldDecorator('password', {
            rules: [{ required: true, message: '請輸入密碼!' }],
          })(
            <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Password" />
          )}
        </FormItem>
        <FormItem label="密碼確認">
          {getFieldDecorator('passwordcheck', {
            rules: [{
              required: true, message: '請輸入密碼確認!'
            }, {
              validator: this.compareToFirstPassword,
            }],
          })(
            <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="PasswordCheck" />
          )}
        </FormItem>
        <FormItem label="姓名">
          {getFieldDecorator('name', {
            rules: [{ required: true, message: '請輸入姓名!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Name" />
          )}
        </FormItem>
        <FormItem label="生日">
          {getFieldDecorator('birthday', config)(
            <DatePicker
              disabledDate={this.disabledDate}
              style={{ width: '100%' }} placeholder="Birth" />
          )}
        </FormItem>
        <FormItem label="性別">
          {getFieldDecorator('sex', { rules: [{ required: true, message: '請選擇性別!' }], initialValue: this.state.sex })(
            <Radio.Group style={{ width: '100%', textAlign: 'center' }} onChange={this.handleonChange}>
              <Radio.Button key={1} value={1} style={{ width: '50%' }} className={this.state.sex === 1 ? `${styles.bluebtn} ${styles.bluebtn_focus}` : styles.bluebtn}>男</Radio.Button>
              <Radio.Button key={2} value={2} style={{ width: '50%' }} className={this.state.sex === 2 ? `${styles.redbtn} ${styles.redbtn_focus}` : styles.redbtn}>女</Radio.Button>
            </Radio.Group>
          )}
        </FormItem>
        <FormItem label="電話">
          {getFieldDecorator('cellphone', {
            rules: [{ required: true, message: '請輸入電話!' }, { max: 10, min: 10, message: '請輸入正確的電話號碼' }],
          })(
            <Input prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Phone" />
          )}
        </FormItem>
        <FormItem style={{ 'paddingTop': '20px', margin: '0' }} >
          <Button type="primary" htmlType="submit" className={styles.registerformbutton}>
            註冊
          </Button>
        </FormItem>
      </Form>
    )
  }

}
export default Form.create()(Register);

import React, { Component } from 'react';
import { Form, DatePicker, Button } from 'antd';
import _ from 'lodash';
const FormItem = Form.Item;
const { RangePicker } = DatePicker;

class DateRangePicker extends Component {

  rangeChange = (e) => {
    e.preventDefault();
    const { handleChange } = this.props;
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        const rangeValue = fieldsValue['rangepicker'];
        !_.isEmpty(rangeValue) ? this.momentchange(rangeValue) : handleChange(fieldsValue);
      }
      else {
        console.log(err);
      }
    })
  }

  momentchange = (rangeValue) => {
    const { handleChange } = this.props;
    const values = {
      rangepicker: {
        date1: rangeValue[0].format('YYYY-MM-DD'), date2: rangeValue[1].format('YYYY-MM-DD')
      }
    }
    handleChange(values);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        md: { span: 24 },
        lg: { span: 12 },
      },
      wrapperCol: {
        md: { span: 24 },
        lg: { span: 24 },
      },
    };
    // const rangeConfig = {
    //   rules: [{ type: 'array', required: false, message: 'Please select time!' }],
    // };
    return (
      <Form layout="inline" onSubmit={this.rangeChange}>
        <FormItem
          {...formItemLayout}
        >
          {getFieldDecorator('rangepicker')(
            <RangePicker />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
        >
          <Button type="primary" htmlType="submit" onSubmit={this.rangeChange}>
            搜尋
          </Button>
        </FormItem>
      </Form>
    )
  }
}
export default Form.create()(DateRangePicker);

import React, { Component } from 'react';
import { Form, Row, Icon, Input, Checkbox, Button } from 'antd';
import styles from './Login.css';
import _ from 'lodash';
const FormItem = Form.Item;

class Login extends Component {

  state = {

    name: '',
    type: '',
    required: '',
  }

  handleSubmit=(e) =>{
    const { handleSubmit } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const callback = () => {
          this.props.form.resetFields();
        }

        handleSubmit(values, callback);
      } else {
        console.log(err);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className={styles.loginform} >
        <h2 className={styles.formtitle}><strong>登入</strong></h2>
        <FormItem>
          {getFieldDecorator('account', {
            rules: [{ required: true, message: '請輸入帳號' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="帳號" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: '請輸入密碼!' }],
          })(
            <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="密碼" />
          )}
        </FormItem>
        <FormItem style={{'paddingTop': '10px', margin: '0'}} >
          <a className={styles.loginformforgot} onClick={this.props.underLoginshowForgetPassModal}>忘記密碼</a>
          <a onClick={this.props.underLoginshowRegisterModal}>馬上加入我們</a>
          <Button type="primary" htmlType="submit" className={styles.loginformbutton}>
            登入
          </Button>

        </FormItem>
      </Form>
    )
  }

}
export default Form.create()(Login);

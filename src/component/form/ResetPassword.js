import React, { Component } from 'react';
import { Form, Row, Icon, Input, Col, Button, DatePicker, Radio } from 'antd';
import styles from './Profile.css';
import moment from 'moment'
import _ from 'lodash';
const FormItem = Form.Item;

class ResetPassword extends Component {

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('密碼不一致!');
    } else {
      callback();
    }
  }

  handleSubmit = (e) => {
    const { handleResetSubmit } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const callback = () => {
          this.props.form.resetFields();
        }

        handleResetSubmit(values, callback);
      } else {
        console.log(err);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className={styles.loginform} >
        <h2 className={styles.formtitle}><strong>驗證成功!</strong></h2>
        <h2 className={styles.formtitle}><strong>接下來輸入驗證碼及設定新的密碼</strong></h2>
        <FormItem label="驗證碼">
          {getFieldDecorator('authcode', {
            rules: [{ required: true, message: '請輸入驗證碼!' }],
          })(
            <Input type="text" prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Validate" />
          )}
        </FormItem>
        <FormItem label="密碼">
          {getFieldDecorator('password', {
            rules: [{ required: true, message: '請輸入密碼!' }],
          })(
            <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="OldPassword" />
          )}
        </FormItem>
        <FormItem label="新密碼確認">
          {getFieldDecorator('passwordcheck', {
            rules: [{
              required: true, message: '請輸入密碼確認!'
            }, {
              validator: this.compareToFirstPassword,
            }],
          })(
            <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="NewPasswordCheck" />
          )}
        </FormItem>
        <FormItem style={{ 'paddingTop': '10px', margin: '0' }} >
          <Button type="primary" htmlType="submit" >
            送出
          </Button>
        </FormItem>
      </Form>
    )
  }
}
export default Form.create()(ResetPassword);

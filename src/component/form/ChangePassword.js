import React, { Component } from 'react';
import { Form, Row, Icon, Input, Col, Button, DatePicker, Radio } from 'antd';
import styles from './Profile.css';
import moment from 'moment'
import _ from 'lodash';
const FormItem = Form.Item;

class ChangePassword extends Component {
  state = {
    name: '',
    type: '',
    required: '',
    isloading: false,
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('新密碼不一致!');
    } else {
      callback();
    }
  }

  handleSubmit = (e) => {
    const { handleChange } = this.props;
    const { userData } = this.props;
    console.log(this.props);
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values = {...values, account: userData.account}
        handleChange(values);
      } else {
        console.log(err);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form onSubmit={this.handleSubmit} style={{ minHeight: '1200px' }} >
          <Row>
            <Col span={14}>
              <Row>
                <Col span={24}>
                  <FormItem label="舊密碼">
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: '請輸入密碼!' }],
                    })(
                      <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="OldPassword" />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="新密碼">
                    {getFieldDecorator('newPassword', {
                      rules: [{
                        required: true, message: '請輸入密碼確認!'
                      }],
                    })(
                      <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="NewPassword" />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="新密碼確認">
                    {getFieldDecorator('newpasswordcheck', {
                      rules: [{
                        required: true, message: '請輸入密碼確認!'
                      }, {
                        validator: this.compareToFirstPassword,
                      }],
                    })(
                      <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="NewPasswordCheck" />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem style={{ 'paddingTop': '20px', margin: '0' }} >
                    <Button loading={this.props.isloading} type="primary" htmlType="submit" className={styles.registerformbutton}>
                      修改確認
                    </Button>
                  </FormItem>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

export default Form.create()(ChangePassword);

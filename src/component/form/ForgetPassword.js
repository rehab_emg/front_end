import React, { Component } from 'react';
import { Form, Row, Icon, Input, Col, Button, DatePicker, Radio } from 'antd';
import styles from './Profile.css';
import moment from 'moment'
import _ from 'lodash';
const FormItem = Form.Item;

class ForgetPassword extends Component {

  handleSubmit=(e) =>{
    const { handleForgetSubmit } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const callback = () => {
          this.props.form.resetFields();
        }

        handleForgetSubmit(values, callback);
      } else {
        console.log(err);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className={styles.loginform} >
        <h2 className={styles.formtitle}><strong>忘記密碼</strong></h2>
        <FormItem label="帳號">
          {getFieldDecorator('account', {
            rules: [{ required: true, message: '請輸入帳號' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Account" />
          )}
        </FormItem>
        <FormItem label="電子信箱">
          {getFieldDecorator('email', {
            rules: [{type: 'email', message: '您輸入的不是電子信箱!'}, { required: true, message: '請輸入電子信箱', }],
          })(
            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
          )}
        </FormItem>
        <FormItem style={{'paddingTop': '10px', margin: '0'}} >
          <Button type="primary" htmlType="submit">
            送出
          </Button>
        </FormItem>
      </Form>
    )
  }

}
export default Form.create()(ForgetPassword);

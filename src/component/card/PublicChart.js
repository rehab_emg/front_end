import React, { Component } from 'react';
import { Row, Col } from 'antd';
import style from './index.css';
import G2 from '@antv/g2';

class PublicChart extends Component {

  state = {
    data: [{
      year: '18~30 歲',
      '人數': 10
    }, {
      year: '31~40 歲',
      '人數': 40
    }, {
      year: '41~50 歲',
      '人數': 61
    }, {
      year: '51~60 歲',
      '人數': 80
    }, {
      year: '61~70 歲',
      '人數': 48
    }, {
      year: '71~80 歲',
      '人數': 38
    }]
  }

  componentDidMount() {
    var chart = new G2.Chart({
      container: 'mountNode',
      forceFit: true,
      height: 250
    });
    chart.source(this.state.data);
    chart.scale('sales', {
      tickInterval: 20
    });
    chart.interval().position('year*人數');
    chart.render();
  }

  render() {
    return (
      <div className={style.block} >
        <Row className={style.header} >
          <div className={style.title}>
            {this.state.title}
          </div>
        </Row>
        <Row>
          <Col span={10}>
            <div id="mountNode"></div>
          </Col>
        </Row>
      </div>
    )
  }

}

export default (PublicChart);

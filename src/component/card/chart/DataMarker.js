import React, { Component } from 'react';
import styles from './DataMarker.css';
import { Button } from 'antd';
import G2 from '@antv/g2';
import Slider from '@antv/g2-plugin-slider';
import DataSet from '@antv/data-set';
import _ from 'lodash';

class DataMarker extends Component {

  state = {
    start: 0,
    end: 60
  }
  /*

  {
    data: [{lr: LR, id: index, value: item},]
  }

  */
  getEmgData = () => {
    const { emgData } = this.props;
    console.log(emgData);
    if (!_.isEmpty(emgData)) {
      // let arr = [];
      // _.map(emgData, (LR) => {
      //   _.map(emgData.data, (item, index) => {
      //     arr.push({ lr: LR, id: index + 1, value: item })
      //   })
      // })

      // _.merge(emgData[0].data, emgData[1].data);
      // console.log([...emgData[0].data, ...emgData[1].data]);

      let result = [];
      _.map(emgData, (data, index) => {
        _.map(data.data, (value, index2) => {
          result.push({
            data: `${data.isLR}-${data.part}-${data.place}`,
            id: index2 + 1,
            value,
          });
        });
      })
      console.log(result);
      this.renderChart(result)
    }
  }

  // controlState = (target, value) => {
  //   this.setState({
  //     [target]: value,
  //   })
  // }

  componentDidMount() {
    this.getEmgData();
  }

  renderChart = (data) => {
    // !!! 创建 DataSet，并设置状态量 start end
    const ds = new DataSet({
      state: {
        start: 0,
        end: 60
      }
    });
    // !!! 通过 ds 创建 DataView
    const dv = ds.createView();
    dv.source(data)
      .transform({ // !!! 根据状态量设置数据过滤规则，
        type: 'filter',
        callback: obj => {
          return obj.id <= ds.state.end && obj.id >= ds.state.start;
        }
      });

    const chart = new G2.Chart({
      id: 'DataMarker',
      forceFit: true,
      // height: 400,
      animate: false
    });
    chart.tooltip({
      crosshairs: {
        type: 'line'
      },
    });
    chart.scale({
      'id': {
        alias: '秒數',
        range: [0, 1]
      }, 'value': {
        min: 0,
        alias: 'sEMG'
      }
    });
    chart.line().position('id*value').color('data', [ '#D51E2C', '#104CA4']).style({
      lineWidth: 1
    });
    // for(var i=0; i<emgData[0].data.length; i=i+60){
    //   chart.guide().dataMarker({
    //     position: [i, 0],
    //     lineLength: 500,
    //     style: {
    //       text: {
    //         textAlign: 'left',
    //         stroke: '#fff',

    //       }
    //     }
    //   });
    // }

    // const view1 = chart.view({
    //   start: {
    //     x: 0,
    //     y: 0
    //   },
    //   end: {
    //     x: 1,
    //     y: 0.45
    //   }
    // });
    // view1.source(dv);  // !!! 注意数据源是 ds 创建 DataView 对象
    chart.source(dv);

    chart.render();

    // var chart = new G2.Chart({
    //   container: 'mountNode',
    //   forceFit: true,
    //   height: 400,
    //   animate: true,
    //   fontSize: 16
    // });
    // chart.source(data);
    // //chart.source(this.state.data);
    // chart.scale({
    //   'id': {
    //     alias: '秒數'
    //   }, 'value': {
    //     min: 0,
    //     alias: 'sEMG'
    //   }
    // });
    // chart.line().position('id*value');
    // // chart.guide().dataMarker({
    // //   position: [347, 522],
    // //   content: '本次復健最高值',
    // //   style: {
    // //     text: {
    // //       textAlign: 'left'
    // //     }
    // //   }
    // // });
    // chart.render();

    // !!! slider 對象
    const slider = new Slider({
      container: 'slider',
      height: 75,
      start: 0,
      end: 60,
      data, // !!! 注意是原始数据，不要传入 dv
      xAxis: 'id',
      yAxis: 'value',
      onChange: ({ startText, endText }) => {
        // !!! 更新状态量
        ds.setState('start', startText);
        ds.setState('end', endText);
      }
    });
    slider.render();
  }

  render() {
    const { emgData } = this.props;
    return (
      _.isEmpty(emgData) ? (<div> <Button size="large" target="_blank" href="https://drive.google.com/file/d/1SgepfgThZ5S5gpeLpTfSt9PDBb_TeZM9/view" style={{ display: 'block', margin: '0 auto' }}>您尚未進行復健，快來下載App!</Button> </div>) :
        (
          <div>
          <h3>{emgData[0].date}</h3>
            <div id="DataMarker" className={styles.charts}></div>
            <div id="slider"></div>
          </div>
        )
    )
  }
}

export default (DataMarker);



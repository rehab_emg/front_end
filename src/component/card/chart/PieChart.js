import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './PieChart.css';
import G2 from '@antv/g2';
import _ from 'lodash';

class PieChart extends Component {

  state = {
    title: '',
    data: [{
      item: '19歲以下',
      count: 20,
      percent: 0.2
    }, {
      item: '20~39歲',
      count: 26,
      percent: 0.26
    }, {
      item: '40~64歲',
      count: 30,
      percent: 0.3
    }, {
      item: '65歲以上',
      count: 34,
      percent: 0.34
    }]
  }

  getAgeData = () => {
    const { AgeFrequency } = this.props;
    if (!_.isEmpty(AgeFrequency)) {
      let arr = [];
      let total = 0;
      _.map(AgeFrequency, (item, index) => {
        total += item[0]
      })
      _.map(AgeFrequency, (item, index) => {
        arr.push({ item: index, count: item[0], percent: item[1] })
      })
      console.log(total);
      console.log(arr);
      this.renderChart(arr, total)
    }
  }

  componentDidMount() {
    this.getAgeData();
  }

  renderChart = (data, total) => {
    var chart = new G2.Chart({
      container: 'PieChart',
      forceFit: true,
      height: 400,
      animate: true
    });
    chart.forceFit();
    chart.source(data, {

    });
    chart.coord('theta', {
      radius: 0.5,
      innerRadius: 0.5
    });
    chart.tooltip({
      showTitle: false,
      itemTpl: '<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
    });
    // 辅助文本
    chart.guide().html({
      position: ['50%', '50%'],
      html: `<div style="color:#8c8c8c;font-size: 14px;text-align: center;width: 10em;">總會員數<br><span style="color:#8c8c8c;font-size:20px">${total}</span>名</div>`,
      alignX: 'middle',
      alignY: 'middle'
    });
    var interval = chart.intervalStack().position('percent').color('item').label('count', {
      formatter: function formatter(count, item) {
        console.log(count);
        return item.point.item + ': ' + count;
      }
    }).tooltip('item*percent', function (item, percent) {
      percent = percent * 100 + '%';
      return {
        name: item,
        value: percent
      };
    }).style({
      lineWidth: 2,
      stroke: '#fff'
    });
    chart.render();
    interval.setSelected(this.state.data[0]);
  }

  render() {

    return (
      <div id="PieChart" className={styles.charts}></div>
    )
  }
}
export default (PieChart);

import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './BasicLineChart.css';
import G2 from '@antv/g2';
import Slider from '@antv/g2-plugin-slider';
import DataSet from '@antv/data-set';
import _ from 'lodash';

class BasicLineChart extends Component {

  state = {
    chart: null,
    slider: null,
    ds: null,
    // data: [{
    //   year: '1991',
    //   value: 3
    // }, {
    //   year: '1992',
    //   value: 4
    // }, {
    //   year: '1993',
    //   value: 3.5
    // }, {
    //   year: '1994',
    //   value: 5
    // }, {
    //   year: '1995',
    //   value: 4.9
    // }, {
    //   year: '1996',
    //   value: 6
    // }, {
    //   year: '1997',
    //   value: 7
    // }, {
    //   year: '1998',
    //   value: 9
    // }, {
    //   year: '1999',
    //   value: 13
    // }]
  }

  getEmgData = (recordData) => {
    if (!_.isEmpty(recordData)) {
      let arr = [];
      _.map(recordData, (item, index) => {
        arr.push({ id: index + 1, value: item })
      })
      //console.log(arr);
      this.renderChart(arr)
    }
  }

  // controlState = (target, value) => {
  //   this.setState({
  //     [target]: value,
  //   })
  // }

  componentDidMount() {

    this.chart = new G2.Chart({
      container: 'ListDrawerChart',
      forceFit: true,
      height: 400,
      animate: false
    });

    this.getEmgData(this.props.recordData);

  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if(this.props.recordData !== nextProps.recordData) {
      this.slider.destroy();
      this.getEmgData(nextProps.recordData);
    }
  }

  renderChart = (data) => {
    // mountNode2

    if(!_.isNull(this.chart)){
      this.chart.destroy();

    }

    // if(!_.isNull(this.slider)){
    //   this.slider.destroy();
    // }

     // !!! 创建 DataSet，并设置状态量 start end
    var ds = new DataSet({
      state: {
        start: 0,
        end: 60
      }
    });
    // !!! 通过 ds 创建 DataView
    const dv = ds.createView();
    dv.source(data)
      .transform({ // !!! 根据状态量设置数据过滤规则，
        type: 'filter',
        callback: obj => {
          return obj.id <= ds.state.end && obj.id >= ds.state.start;
        }
      });

    this.chart = new G2.Chart({
      container: 'ListDrawerChart',
      forceFit: true,
      height: 400
    });
    this.chart.changeData(data)
    this.chart.forceFit();
    this.chart.source(dv);
    this.chart.scale('value', {
      min: 0
    });
    this.chart.scale('id', {
      range: [0, 1]
    });
    this.chart.tooltip({
      crosshairs: {
        type: 'line'
      }
    });
    this.chart.line().position('id*value').style({
      lineWidth: 1
    });
    //this.chart.point().position('id*value').size(1).shape('circle');

    for(var i=0; i<data.length; i=i+60){
      this.chart.guide().dataMarker({
        position: [i, 0],
        lineLength: 500,
        style: {
          text: {
            textAlign: 'left',
            stroke: '#fff',

          }
        }
      });
    }

    this.chart.render();

    // !!! 创建 slider 对象
    this.slider = new Slider({
      container: 'Drawerslider',
      height: 75,
      start: 0,
      end: 60,
      data, // !!! 注意是原始数据，不要传入 dv
      xAxis: 'id',
      yAxis: 'value',
      onChange: ({ startText, endText }) => {
        // !!! 更新状态量
        ds.setState('start', startText);
        ds.setState('end', endText);
      }
    });


    this.slider.render();

  }

  render() {

    return (
      <div>
      <div id="ListDrawerChart" className={styles.charts}></div>
        <div id="Drawerslider"></div>
      </div>

    )
  }
}
export default (BasicLineChart);

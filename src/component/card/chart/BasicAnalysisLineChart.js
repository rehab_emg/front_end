import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './BasicLineChart.css';
import G2 from '@antv/g2';
import Slider from '@antv/g2-plugin-slider';
import DataSet from '@antv/data-set';
import _ from 'lodash';

class BasicAnalysisLineChart extends Component {


  getEmgData = () => {
    const { data, container } = this.props;
    if (!_.isEmpty(data)) {
      // let arr = [];
      // _.map(emgData, (LR) => {
      //   _.map(emgData.data, (item, index) => {
      //     arr.push({ lr: LR, id: index + 1, value: item })
      //   })
      // })

      // _.merge(emgData[0].data, emgData[1].data);
      // console.log([...emgData[0].data, ...emgData[1].data]);

      let result = [];
      // _.map(data, (data, index) => {
      //   _.map(data.data, (value, index2) => {
      //     result.push({
      //       data: `${data.isLR}-${data.part}-${data.place}`,
      //       id: index2 + 1,
      //       value,
      //     });
      //   });
      // })

      if(container === 'mf'){
        _.map(data, (data, index) => {
          _.map(data.mf, (value, index2) => {
            result.push({
              data: `${data.isLR}-${data.part}-${data.place}`,
              id: index2 + 1,
              value,
            });
          });
        })
      }
      else if(container === 'mpf'){
        _.map(data, (data, index) => {
          _.map(data.mpf, (value, index2) => {
            result.push({
              data: `${data.isLR}-${data.part}-${data.place}`,
              id: index2 + 1,
              value,
            });
          });
        })
      }
      else if(container === 'rms'){
        _.map(data, (data, index) => {
          _.map(data.rms, (value, index2) => {
            result.push({
              data: `${data.isLR}-${data.part}-${data.place}`,
              id: index2 + 1,
              value,
            });
          });
        })
      }
      else if(container === 'record_rms'){
          _.map(data.rms, (value, index2) => {
            result.push({
              data: `${data.isLR}-${data.part}-${data.place}`,
              id: index2 + 1,
              value,
            });
          });
      }
      else if(container === 'record_mpf'){
          _.map(data.mpf, (value, index2) => {
            result.push({
              data: `${data.isLR}-${data.part}-${data.place}`,
              id: index2 + 1,
              value,
            });
          });
      }
      else if(container === 'record_mf'){
          _.map(data.mf, (value, index2) => {
            result.push({
              data: `${data.isLR}-${data.part}-${data.place}`,
              id: index2 + 1,
              value,
            });
          });
      }

      this.renderChart(result)
    }
  }

  // controlState = (target, value) => {
  //   this.setState({
  //     [target]: value,
  //   })
  // }

  componentDidMount() {
    this.getEmgData();
  }

  renderChart = (data) => {
    const { container } = this.props;
    // !!! 创建 DataSet，并设置状态量 start end
    var chart = new G2.Chart({
      container: container,
      forceFit: true,
      height: 200,
      padding: [20, 110, 70, 35]
    });
    chart.source(data);
    chart.scale('id', {
      range: [0, 1],
      tickCount: 10,
    });
    chart.axis('data', {
      label: {
        textStyle: {
          fill: '#aaaaaa'
        }
      }
    });
    chart.axis('value', {
      label: {
        textStyle: {
          fill: '#aaaaaa'
        }
      }
    });
    chart.tooltip({
      crosshairs: 'x',
      share: true
    });
    chart.legend({
      attachLast: true
    });
    chart.line().position('id*value').color('data', ['#104CA4']).style({
      lineWidth: 1
    });
    chart.render();
  }

  render() {
    const{ container }=this.props
    return (
      <div>
        <div id={container} className={styles.charts} style={{fontSize: '16px'}}></div>
      </div>
    )
  }
}
export default (BasicAnalysisLineChart);

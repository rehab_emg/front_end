import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './BasicBarChart.css';
import G2 from '@antv/g2';
import _ from 'lodash';

class BasicBarChart extends Component {

  state = {
    data: null
  }

  getPartData = () => {
    const { PartFrequency } = this.props;
    //console.log(PartFrequency)
    if (!_.isEmpty(PartFrequency)) {
      let arr = [];
      _.map(PartFrequency, (item) => {
        //console.log(item)
        arr.push({ label: item.part, type:item.isLR, value: item.count })
      })
      //console.log(arr);
      this.renderChart(arr)
    }
  }

  componentDidMount() {
    this.getPartData();
  }

  renderChart(data) {
    // mountNode2
    //console.log(data)
    var chart = new G2.Chart({
      container: 'BasicBarChart',
      forceFit: true,
      height: 400
    });
    chart.source(data);
    chart.axis('value', {
      position: 'right'
    });
    chart.axis('label', {
      label: {
        offset: 12
      }
    });
    chart.coord().transpose().scale(1, -1);
    chart.interval().position('label*value').color('type', [ '#1f77b4', '#ff6e19']).adjust([{
      type: 'dodge',
      marginRatio: 1 / 8
    }]);
    chart.render();
  }

  render() {

    return (
      <div>
        <div id="BasicBarChart" className={styles.charts} style={{fontSize: '16px'}}></div>
      </div>
    )
  }

}
export default (BasicBarChart);

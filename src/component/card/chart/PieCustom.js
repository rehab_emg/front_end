import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './PieChart.css';
import G2 from '@antv/g2';
import _ from 'lodash';

class PieCustom extends Component {

  state = {
    data: [{
      sex: '男',
      sold: 0.45
    }, {
      sex: '女',
      sold: 0.55
    }]
  }

  getSexData = () => {
    const { SexFrequency } = this.props;
    console.log(SexFrequency);
    if (!_.isEmpty(SexFrequency)) {
      let arr = [];
      let total = 0;
      _.map(SexFrequency, (item, index) => {
        total += item
      })
      _.map(SexFrequency, (item, index) => {
        arr.push({ sex: index, sold: item/total })
      })
      console.log(total);
      console.log(arr);
      this.renderChart(arr);
    }
  }

  componentDidMount() {
    this.getSexData();
  }

  renderChart = (arr) => {
    const Shape = G2.Shape;
    Shape.registerShape('interval', 'radiusPie', {
      draw: function draw(cfg, container) {
        // 将归一化后的数据转换为画布上的坐标
        var points = cfg.origin.points;
        var path = [];
        for (var i = 0; i < cfg.origin.points.length; i += 4) {
          path.push(['M', points[i].x, points[i].y]);
          path.push(['L', points[i + 1].x, points[i + 1].y]);
          path.push(['L', points[i + 2].x, points[i + 2].y]);
          path.push(['L', points[i + 3].x, points[i + 3].y]);
          path.push(['L', points[i].x, points[i].y]);
          path.push(['z']);
        }
        path = this.parsePath(path, true);
        var rect = container.addShape('path', {
          attrs: {
            fill: cfg.color || '#00D9DF',
            path: path
          }
        });
        var minH = Math.min(path[1][7], path[2][2]);
        var minW = Math.min(path[1][6], path[2][1]);
        var diffH = Math.abs(path[1][7] - path[2][2]);
        var diffW = Math.abs(path[1][6] - path[2][1]);
        container.addShape('circle', {
          attrs: {
            x: minW + diffW / 2,
            y: minH + diffH / 2,
            fill: cfg.color,
            radius: diffH / 2
          }
        });
        return rect;
      }
    });

    var chart = new G2.Chart({
      container: 'piecustom',
      forceFit: true,
      height: 400,
      padding: [0, 50, 0, 100]
    });

    var COLORS = ['#1890ff', '#f04864'];

    chart.coord('theta', {
      radius: 0.8
    });
    chart.source(arr);
    chart.tooltip({
      showTitle: false
    });
    chart.intervalStack().position('sold').shape('radiusPie').color('sex', COLORS).label('sold', {
      custom: true,
      htmlTemplate: function htmlTemplate(text, item) {
        var isFemale = item.point.sex === '女';
        var src = isFemale ? 'https://gw.alipayobjects.com/zos/rmsportal/mweUsJpBWucJRixSfWVP.png' : 'https://gw.alipayobjects.com/zos/rmsportal/oeCxrAewtedMBYOETCln.png';
        var color = isFemale ? COLORS[1] : COLORS[0];
        var IMG = '<img style="width:40px" src="' + src + '" /><br/>';
        return '<div style="text-align:center;color:' + color + '">' + IMG + (text * 100).toFixed(0) + '%</div>';
      }
    });

    chart.render();
  }

  render() {

    return (
      <div id="piecustom" className={styles.charts}></div>
    )
  }

}
export default (PieCustom);

import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './BasicSingleBarChart.css';
import G2 from '@antv/g2';
import _ from 'lodash';

class BasicBarChart extends Component {

  state = {
    data: null
  }

  getLegData = () => {
    const { LegFrequency } = this.props;
   // console.log(LegFrequency)
    if (!_.isEmpty(LegFrequency)) {
      let arr = [];
      let most = 0;
      arr.push({ type:'左腳', value: LegFrequency.left });
      arr.push({ type:'右腳', value: LegFrequency.right });
      LegFrequency.left > LegFrequency.right ? most=LegFrequency.left:  most=LegFrequency.right
      //console.log(arr);
      this.renderChart(arr,most);
    }
  }

  componentDidMount() {
    this.getLegData();
  }

  renderChart(data,most) {
    // mountNode2
    //console.log(data)
    var chart = new G2.Chart({
      container: 'BasicSingleBarChart',
      forceFit: true,
      height: 300,
      padding: [20, 40, 50, 124]
    });
    chart.source(data, {
      value: {
        max: most,
        min: 0,
        nice: false,
        alias: '數量'
      }
    });
    chart.axis('type', {
      label: {
        textStyle: {
          fill: '#8d8d8d',
          fontSize: 12
        }
      },
      tickLine: {
        alignWithLabel: false,
        length: 0
      },
      line: {
        lineWidth: 0
      }
    });
    chart.axis('value', {
      label: null,
      title: {
        offset: 30,
        textStyle: {
          fontSize: 12,
          fontWeight: 300
        }
      }
    });
    chart.legend(false);
    chart.coord().transpose();
    chart.interval().position('type*value').size(26).opacity(1).label('value', {
      textStyle: {
        fill: '#8d8d8d'
      },
      offset: 10
    });
    chart.render();
  }

  render() {

    return (
      <div>
        <div id="BasicSingleBarChart" className={styles.charts} style={{fontSize: '16px'}}></div>
      </div>
    )
  }

}
export default (BasicBarChart);

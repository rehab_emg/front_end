import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './BasicAreaChart.css';
import G2 from '@antv/g2';
import _ from 'lodash';

class Example extends Component {
  state = {
    data: [
      { frequency: 66.71, amplitude: 1.25 },
      { frequency: 128.26, amplitude: 2.5 },
      { frequency: 92.39, amplitude: 3.75 },
      { frequency: 70.60, amplitude: 5 },
      { frequency: 193.98, amplitude: 6.25 },
      { frequency: 168.77, amplitude: 7.5 },
      { frequency: 306.02, amplitude: 8.75 },
      { frequency: 139.00, amplitude: 10 },
      { frequency: 306.02, amplitude: 11.25 },
      { frequency: 168.77, amplitude: 12.5 },
      { frequency: 193.98, amplitude: 13.75 },
      { frequency: 70.60, amplitude: 15 },
      { frequency: 92.39, amplitude: 16.25 },
      { frequency: 128.26, amplitude: 17.5 },
      { frequency: 66.71, amplitude: 18.75 }
      ]
  }

  getFrequencyData = () => {
    const { FrequencyDomain } = this.props;
    //console.log(FrequencyDomain)
    if (!_.isEmpty(FrequencyDomain)) {
      let arr = [];
      _.map(FrequencyDomain, (item, index) => {
        arr.push({ part: index, value: item })
      })
      //console.log(arr);
      this.renderChart(arr)
    }
  }
  componentDidMount() {
    //this.getFrequencyData();
    //this.renderChart();
    //console.log(this.state.data)
    var chart = new G2.Chart({
      container: 'BasicAreaChart',
      forceFit: true,
      height: 400
    });
    chart.source(this.state.data);
    chart.scale({
      frequency: {
        min: 0
      },
      amplitude: {
        range: [0, 1]
      }
    });
    // chart.axis('value', {
    //   label: {
    //     formatter: function formatter(val) {
    //       return (val / 10000).toFixed(1) + 'k';
    //     }
    //   }
    // });
    chart.tooltip({
      crosshairs: {
        type: 'line'
      }
    });
    chart.area().position('amplitude*frequency');
    chart.line().position('amplitude*frequency').size(2);
    chart.render();
  }

  renderChart() {
    // mountNode2
    //console.log(this.state.data)
    var chart = new G2.Chart({
      container: 'BasicAreaChart',
      forceFit: true,
      height: 400
    });
    chart.source(this.state.data);
    chart.scale({
      frequency: {
        min: 0
      },
      amplitude: {
        range: [0, 1]
      }
    });
    // chart.axis('value', {
    //   label: {
    //     formatter: function formatter(val) {
    //       return (val / 10000).toFixed(1) + 'k';
    //     }
    //   }
    // });
    chart.tooltip({
      crosshairs: {
        type: 'line'
      }
    });
    chart.area().position('amplitude*frequency');
    chart.line().position('amplitude*frequency').size(2);
    chart.render();
  }

  render() {
    return (
      <div>
        <div id="BasicAreaChart" className={styles.charts} style={{ fontSize: '16px' }}></div>
      </div>
    )
  }

}
export default (Example);

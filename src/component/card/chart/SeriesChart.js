import React, { Component } from 'react';
import { Row, Col } from 'antd';
import styles from './SeriesChart';
import G2 from '@antv/g2';

class SeriesChart extends Component {

  state = {
    data: [{ user: 'test01', id: 1, value: 302 },
    { user: 'test01', id: 2, value: 318 },
    { user: 'test01', id: 3, value: 303 },
    { user: 'test01', id: 4, value: 312 },
    { user: 'test01', id: 5, value: 309 },
    { user: 'test01', id: 6, value: 304 },
    { user: 'test01', id: 7, value: 316 },
    { user: 'test01', id: 8, value: 300 },
    { user: 'test01', id: 9, value: 317 },
    { user: 'test01', id: 10, value: 302 },
    { user: 'test01', id: 11, value: 311 },
    { user: 'test01', id: 12, value: 309 },
    { user: 'test01', id: 13, value: 303 },
    { user: 'test01', id: 14, value: 316 },
    { user: 'test01', id: 15, value: 300 },
    { user: 'test01', id: 16, value: 316 },
    { user: 'test01', id: 17, value: 302 },
    { user: 'test01', id: 18, value: 310 },
    { user: 'test01', id: 19, value: 309 },
    { user: 'test01', id: 20, value: 303 },
    { user: 'test01', id: 21, value: 315 },
    { user: 'test01', id: 22, value: 299 },
    { user: 'test01', id: 23, value: 316 },
    { user: 'test01', id: 24, value: 302 },
    { user: 'test01', id: 25, value: 310 },
    { user: 'test01', id: 26, value: 312 },
    { user: 'test01', id: 27, value: 302 },
    { user: 'test01', id: 28, value: 319 },
    { user: 'test01', id: 29, value: 301 },
    { user: 'test01', id: 30, value: 318 },
    { user: 'test01', id: 31, value: 306 },
    { user: 'test01', id: 32, value: 317 },
    { user: 'test01', id: 33, value: 313 },
    { user: 'test01', id: 34, value: 304 },
    { user: 'test01', id: 35, value: 317 },
    { user: 'test01', id: 36, value: 302 },
    { user: 'test01', id: 37, value: 308 },
    { user: 'test01', id: 38, value: 299 },
    { user: 'test01', id: 39, value: 305 },
    { user: 'test01', id: 40, value: 314 },
    { user: 'test01', id: 41, value: 313 },
    { user: 'test01', id: 42, value: 332 },
    { user: 'test01', id: 43, value: 318 },
    { user: 'test01', id: 44, value: 327 },
    { user: 'test01', id: 45, value: 310 },
    { user: 'test01', id: 46, value: 314 },
    { user: 'test01', id: 47, value: 314 },
    { user: 'test01', id: 48, value: 304 },
    { user: 'test01', id: 49, value: 322 },
    { user: 'test01', id: 50, value: 303 },
    { user: 'test01', id: 51, value: 304 },
    { user: 'test01', id: 52, value: 301 },
    { user: 'test01', id: 53, value: 303 },
    { user: 'test01', id: 54, value: 294 },
    { user: 'test01', id: 55, value: 293 },
    { user: 'test01', id: 56, value: 312 },
    { user: 'test01', id: 57, value: 286 },
    { user: 'test01', id: 58, value: 302 },
    { user: 'test01', id: 59, value: 303 },
    { user: 'test01', id: 60, value: 313 },
    { user: 'test01', id: 61, value: 320 },
    { user: 'test01', id: 62, value: 330 },
    { user: 'test01', id: 63, value: 351 },
    { user: 'test01', id: 64, value: 347 },
    { user: 'test01', id: 65, value: 328 },
    { user: 'test01', id: 66, value: 322 },
    { user: 'test01', id: 67, value: 317 },
    { user: 'test01', id: 68, value: 303 },
    { user: 'test01', id: 69, value: 298 },
    { user: 'test01', id: 70, value: 307 },
    { user: 'test01', id: 71, value: 293 },
    { user: 'test01', id: 72, value: 302 },
    { user: 'test01', id: 73, value: 295 },
    { user: 'test01', id: 74, value: 301 },
    { user: 'test01', id: 75, value: 306 },
    { user: 'test01', id: 76, value: 299 },
    { user: 'test01', id: 77, value: 316 },
    { user: 'test01', id: 78, value: 300 },
    { user: 'test01', id: 79, value: 313 },
    { user: 'test01', id: 80, value: 301 },
    { user: 'test01', id: 81, value: 297 },
    { user: 'test01', id: 82, value: 295 },
    { user: 'test01', id: 83, value: 287 },
    { user: 'test01', id: 84, value: 304 },
    { user: 'test01', id: 85, value: 292 },
    { user: 'test01', id: 86, value: 307 },
    { user: 'test01', id: 87, value: 300 },
    { user: 'test01', id: 88, value: 297 },
    { user: 'test01', id: 89, value: 303 },
    { user: 'test01', id: 90, value: 285 },
    { user: 'test01', id: 91, value: 306 },
    { user: 'test01', id: 92, value: 281 },
    { user: 'test01', id: 93, value: 295 },
    { user: 'test01', id: 94, value: 283 },
    { user: 'test01', id: 95, value: 286 },
    { user: 'test01', id: 96, value: 300 },
    { user: 'test01', id: 97, value: 290 },
    { user: 'test01', id: 98, value: 311 },
    { user: 'test01', id: 99, value: 297 },
    { user: 'test01', id: 100, value: 314 },
    { user: 'test01', id: 101, value: 309 },
    { user: 'test01', id: 102, value: 305 },
    { user: 'test01', id: 103, value: 320 },
    { user: 'test01', id: 104, value: 302 },
    { user: 'test01', id: 105, value: 321 },
    { user: 'test01', id: 106, value: 306 },
    { user: 'test01', id: 107, value: 322 },
    { user: 'test01', id: 108, value: 314 },
    { user: 'test01', id: 109, value: 308 },
    { user: 'test01', id: 110, value: 323 },
    { user: 'test01', id: 111, value: 311 },
    { user: 'test01', id: 112, value: 328 },
    { user: 'test01', id: 113, value: 315 },
    { user: 'test01', id: 114, value: 332 },
    { user: 'test01', id: 115, value: 344 },
    { user: 'test01', id: 116, value: 334 },
    { user: 'test01', id: 117, value: 330 },
    { user: 'test01', id: 118, value: 309 },
    { user: 'test01', id: 119, value: 314 },
    { user: 'test01', id: 120, value: 297 },
    { user: 'test01', id: 121, value: 295 },
    { user: 'test01', id: 122, value: 295 },
    { user: 'test01', id: 123, value: 294 },
    { user: 'test01', id: 124, value: 302 },
    { user: 'test01', id: 125, value: 289 },
    { user: 'test01', id: 126, value: 320 },
    { user: 'test01', id: 127, value: 302 },
    { user: 'test01', id: 128, value: 316 },
    { user: 'test01', id: 129, value: 318 },
    { user: 'test01', id: 130, value: 310 },
    { user: 'test01', id: 131, value: 311 },
    { user: 'test01', id: 132, value: 303 },
    { user: 'test01', id: 133, value: 320 },
    { user: 'test01', id: 134, value: 303 },
    { user: 'test01', id: 135, value: 306 },
    { user: 'test01', id: 136, value: 306 },
    { user: 'test01', id: 137, value: 292 },
    { user: 'test01', id: 138, value: 322 },
    { user: 'test01', id: 139, value: 306 },
    { user: 'test01', id: 140, value: 318 },
    { user: 'test01', id: 141, value: 317 },
    { user: 'test01', id: 142, value: 332 },
    { user: 'test01', id: 143, value: 352 },
    { user: 'test01', id: 144, value: 351 },
    { user: 'test01', id: 145, value: 397 },
    { user: 'test01', id: 146, value: 374 },
    { user: 'test01', id: 147, value: 368 },
    { user: 'test01', id: 148, value: 329 },
    { user: 'test01', id: 149, value: 293 },
    { user: 'test01', id: 150, value: 282 },
    { user: 'test01', id: 151, value: 263 },
    { user: 'test01', id: 152, value: 273 },
    { user: 'test01', id: 153, value: 264 },
    { user: 'test01', id: 154, value: 281 },
    { user: 'test01', id: 155, value: 281 },
    { user: 'test01', id: 156, value: 286 },
    { user: 'test01', id: 157, value: 293 },
    { user: 'test01', id: 158, value: 273 },
    { user: 'test01', id: 159, value: 287 },
    { user: 'test01', id: 160, value: 265 },
    { user: 'test01', id: 161, value: 279 },
    { user: 'test01', id: 162, value: 272 },
    { user: 'test01', id: 163, value: 278 },
    { user: 'test01', id: 164, value: 300 },
    { user: 'test01', id: 165, value: 292 },
    { user: 'test01', id: 166, value: 314 },
    { user: 'test01', id: 167, value: 298 },
    { user: 'test01', id: 168, value: 314 },
    { user: 'test01', id: 169, value: 308 },
    { user: 'test01', id: 170, value: 308 },
    { user: 'test01', id: 171, value: 317 },
    { user: 'test01', id: 172, value: 299 },
    { user: 'test01', id: 173, value: 316 },
    { user: 'test01', id: 174, value: 298 },
    { user: 'test01', id: 175, value: 310 },
    { user: 'test01', id: 176, value: 307 },
    { user: 'test01', id: 177, value: 302 },
    { user: 'test01', id: 178, value: 314 },
    { user: 'test01', id: 179, value: 301 },
    { user: 'test01', id: 180, value: 323 },
    { user: 'test01', id: 181, value: 303 },
    { user: 'test01', id: 182, value: 319 },
    { user: 'test01', id: 183, value: 321 },
    { user: 'test01', id: 184, value: 310 },
    { user: 'test01', id: 185, value: 320 },
    { user: 'test01', id: 186, value: 302 },
    { user: 'test01', id: 187, value: 308 },
    { user: 'test01', id: 188, value: 290 },
    { user: 'test01', id: 189, value: 293 },
    { user: 'test01', id: 190, value: 294 },
    { user: 'test01', id: 191, value: 288 },
    { user: 'test01', id: 192, value: 303 },
    { user: 'test01', id: 193, value: 294 },
    { user: 'test01', id: 194, value: 323 },
    { user: 'test01', id: 195, value: 326 },
    { user: 'test01', id: 196, value: 337 },
    { user: 'test01', id: 197, value: 335 },
    { user: 'test01', id: 198, value: 320 },
    { user: 'test01', id: 199, value: 332 },
    { user: 'test01', id: 200, value: 320 },
    { user: 'test02', id: 1, value: 299 },
    { user: 'test02', id: 2, value: 313 },
    { user: 'test02', id: 3, value: 315 },
    { user: 'test02', id: 4, value: 302 },
    { user: 'test02', id: 5, value: 296 },
    { user: 'test02', id: 6, value: 307 },
    { user: 'test02', id: 7, value: 317 },
    { user: 'test02', id: 8, value: 308 },
    { user: 'test02', id: 9, value: 295 },
    { user: 'test02', id: 10, value: 301 },
    { user: 'test02', id: 11, value: 315 },
    { user: 'test02', id: 12, value: 312 },
    { user: 'test02', id: 13, value: 297 },
    { user: 'test02', id: 14, value: 297 },
    { user: 'test02', id: 15, value: 311 },
    { user: 'test02', id: 16, value: 316 },
    { user: 'test02', id: 17, value: 303 },
    { user: 'test02', id: 18, value: 295 },
    { user: 'test02', id: 19, value: 305 },
    { user: 'test02', id: 20, value: 316 },
    { user: 'test02', id: 21, value: 308 },
    { user: 'test02', id: 22, value: 295 },
    { user: 'test02', id: 23, value: 299 },
    { user: 'test02', id: 24, value: 314 },
    { user: 'test02', id: 25, value: 314 },
    { user: 'test02', id: 26, value: 304 },
    { user: 'test02', id: 27, value: 308 },
    { user: 'test02', id: 28, value: 314 },
    { user: 'test02', id: 29, value: 316 },
    { user: 'test02', id: 30, value: 300 },
    { user: 'test02', id: 31, value: 292 },
    { user: 'test02', id: 32, value: 308 },
    { user: 'test02', id: 33, value: 333 },
    { user: 'test02', id: 34, value: 352 },
    { user: 'test02', id: 35, value: 336 },
    { user: 'test02', id: 36, value: 319 },
    { user: 'test02', id: 37, value: 329 },
    { user: 'test02', id: 38, value: 325 },
    { user: 'test02', id: 39, value: 328 },
    { user: 'test02', id: 40, value: 342 },
    { user: 'test02', id: 41, value: 381 },
    { user: 'test02', id: 42, value: 483 },
    { user: 'test02', id: 43, value: 536 },
    { user: 'test02', id: 44, value: 481 },
    { user: 'test02', id: 45, value: 378 },
    { user: 'test02', id: 46, value: 309 },
    { user: 'test02', id: 47, value: 263 },
    { user: 'test02', id: 48, value: 228 },
    { user: 'test02', id: 49, value: 198 },
    { user: 'test02', id: 50, value: 196 },
    { user: 'test02', id: 51, value: 206 },
    { user: 'test02', id: 52, value: 199 },
    { user: 'test02', id: 53, value: 207 },
    { user: 'test02', id: 54, value: 231 },
    { user: 'test02', id: 55, value: 258 },
    { user: 'test02', id: 56, value: 253 },
    { user: 'test02', id: 57, value: 242 },
    { user: 'test02', id: 58, value: 267 },
    { user: 'test02', id: 59, value: 284 },
    { user: 'test02', id: 60, value: 284 },
    { user: 'test02', id: 61, value: 278 },
    { user: 'test02', id: 62, value: 284 },
    { user: 'test02', id: 63, value: 301 },
    { user: 'test02', id: 64, value: 300 },
    { user: 'test02', id: 65, value: 288 },
    { user: 'test02', id: 66, value: 293 },
    { user: 'test02', id: 67, value: 308 },
    { user: 'test02', id: 68, value: 315 },
    { user: 'test02', id: 69, value: 300 },
    { user: 'test02', id: 70, value: 285 },
    { user: 'test02', id: 71, value: 304 },
    { user: 'test02', id: 72, value: 305 },
    { user: 'test02', id: 73, value: 306 },
    { user: 'test02', id: 74, value: 301 },
    { user: 'test02', id: 75, value: 313 },
    { user: 'test02', id: 76, value: 330 },
    { user: 'test02', id: 77, value: 329 },
    { user: 'test02', id: 78, value: 315 },
    { user: 'test02', id: 79, value: 333 },
    { user: 'test02', id: 80, value: 428 },
    { user: 'test02', id: 81, value: 521 },
    { user: 'test02', id: 82, value: 483 },
    { user: 'test02', id: 83, value: 390 },
    { user: 'test02', id: 84, value: 308 },
    { user: 'test02', id: 85, value: 240 },
    { user: 'test02', id: 86, value: 223 },
    { user: 'test02', id: 87, value: 222 },
    { user: 'test02', id: 88, value: 230 },
    { user: 'test02', id: 89, value: 241 },
    { user: 'test02', id: 90, value: 242 },
    { user: 'test02', id: 91, value: 240 },
    { user: 'test02', id: 92, value: 257 },
    { user: 'test02', id: 93, value: 279 },
    { user: 'test02', id: 94, value: 287 },
    { user: 'test02', id: 95, value: 277 },
    { user: 'test02', id: 96, value: 281 },
    { user: 'test02', id: 97, value: 298 },
    { user: 'test02', id: 98, value: 311 },
    { user: 'test02', id: 99, value: 302 },
    { user: 'test02', id: 100, value: 291 },
    { user: 'test02', id: 101, value: 299 },
    { user: 'test02', id: 102, value: 314 },
    { user: 'test02', id: 103, value: 310 },
    { user: 'test02', id: 104, value: 295 },
    { user: 'test02', id: 105, value: 301 },
    { user: 'test02', id: 106, value: 318 },
    { user: 'test02', id: 107, value: 310 },
    { user: 'test02', id: 108, value: 296 },
    { user: 'test02', id: 109, value: 295 },
    { user: 'test02', id: 110, value: 311 },
    { user: 'test02', id: 111, value: 326 },
    { user: 'test02', id: 112, value: 319 },
    { user: 'test02', id: 113, value: 311 },
    { user: 'test02', id: 114, value: 320 },
    { user: 'test02', id: 115, value: 335 },
    { user: 'test02', id: 116, value: 328 },
    { user: 'test02', id: 117, value: 394 },
    { user: 'test02', id: 118, value: 489 },
    { user: 'test02', id: 119, value: 474 },
    { user: 'test02', id: 120, value: 371 },
    { user: 'test02', id: 121, value: 267 },
    { user: 'test02', id: 122, value: 223 },
    { user: 'test02', id: 123, value: 232 },
    { user: 'test02', id: 124, value: 241 },
    { user: 'test02', id: 125, value: 235 },
    { user: 'test02', id: 126, value: 232 },
    { user: 'test02', id: 127, value: 248 },
    { user: 'test02', id: 128, value: 267 },
    { user: 'test02', id: 129, value: 274 },
    { user: 'test02', id: 130, value: 271 },
    { user: 'test02', id: 131, value: 279 },
    { user: 'test02', id: 132, value: 300 },
    { user: 'test02', id: 133, value: 304 },
    { user: 'test02', id: 134, value: 291 },
    { user: 'test02', id: 135, value: 290 },
    { user: 'test02', id: 136, value: 307 },
    { user: 'test02', id: 137, value: 313 },
    { user: 'test02', id: 138, value: 305 },
    { user: 'test02', id: 139, value: 299 },
    { user: 'test02', id: 140, value: 308 },
    { user: 'test02', id: 141, value: 320 },
    { user: 'test02', id: 142, value: 316 },
    { user: 'test02', id: 143, value: 295 },
    { user: 'test02', id: 144, value: 301 },
    { user: 'test02', id: 145, value: 315 },
    { user: 'test02', id: 146, value: 319 },
    { user: 'test02', id: 147, value: 316 },
    { user: 'test02', id: 148, value: 322 },
    { user: 'test02', id: 149, value: 332 },
    { user: 'test02', id: 150, value: 342 },
    { user: 'test02', id: 151, value: 446 },
    { user: 'test02', id: 152, value: 553 },
    { user: 'test02', id: 153, value: 459 },
    { user: 'test02', id: 154, value: 329 },
    { user: 'test02', id: 155, value: 232 },
    { user: 'test02', id: 156, value: 205 },
    { user: 'test02', id: 157, value: 211 },
    { user: 'test02', id: 158, value: 231 },
    { user: 'test02', id: 159, value: 236 },
    { user: 'test02', id: 160, value: 231 },
    { user: 'test02', id: 161, value: 248 },
    { user: 'test02', id: 162, value: 281 },
    { user: 'test02', id: 163, value: 292 },
    { user: 'test02', id: 164, value: 284 },
    { user: 'test02', id: 165, value: 280 },
    { user: 'test02', id: 166, value: 292 },
    { user: 'test02', id: 167, value: 306 },
    { user: 'test02', id: 168, value: 305 },
    { user: 'test02', id: 169, value: 297 },
    { user: 'test02', id: 170, value: 304 },
    { user: 'test02', id: 171, value: 315 },
    { user: 'test02', id: 172, value: 319 },
    { user: 'test02', id: 173, value: 302 },
    { user: 'test02', id: 174, value: 298 },
    { user: 'test02', id: 175, value: 308 },
    { user: 'test02', id: 176, value: 324 },
    { user: 'test02', id: 177, value: 316 },
    { user: 'test02', id: 178, value: 314 },
    { user: 'test02', id: 179, value: 339 },
    { user: 'test02', id: 180, value: 333 },
    { user: 'test02', id: 181, value: 382 },
    { user: 'test02', id: 182, value: 426 },
    { user: 'test02', id: 183, value: 462 },
    { user: 'test02', id: 184, value: 461 },
    { user: 'test02', id: 185, value: 435 },
    { user: 'test02', id: 186, value: 378 },
    { user: 'test02', id: 187, value: 342 },
    { user: 'test02', id: 188, value: 344 },
    { user: 'test02', id: 189, value: 333 },
    { user: 'test02', id: 190, value: 275 },
    { user: 'test02', id: 191, value: 276 },
    { user: 'test02', id: 192, value: 286 },
    { user: 'test02', id: 193, value: 295 },
    { user: 'test02', id: 194, value: 287 },
    { user: 'test02', id: 195, value: 260 },
    { user: 'test02', id: 196, value: 277 },
    { user: 'test02', id: 197, value: 273 },
    { user: 'test02', id: 198, value: 277 },
    { user: 'test02', id: 199, value: 258 },
    { user: 'test02', id: 200, value: 261 },
    ]
    // data: null
  }


  componentDidMount() {
    var chart = new G2.Chart({
      container: 'seriesChart',
      forceFit: true,
      height: 400,
      animation: true,
      fill: '#000000',
    });
    chart.source(this.state.data, {
      id: {
        range: [0, 1]
      }
    });
    chart.tooltip({
      crosshairs: {
        type: 'line'
      }
    });
    chart.line().position('id*value').color('user', ['#D51E2C', '#104CA4']).style({
      lineWidth: 3
    });
    // chart.point().position('id*value').color('user').size(0).shape('circle').style({
    //   stroke: '#fff',
    //   lineWidth: 1
    // });
    chart.render();
  }

  render() {
    return (
      <div>
        <div id="seriesChart" className={styles.charts}></div>
      </div>
    )
  }

}
export default (SeriesChart);

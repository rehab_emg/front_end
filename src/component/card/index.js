import React, { Component } from 'react';
import { Row } from 'antd';
import style from './index.css';

class Index extends Component {

  state = {
    // title: '個人化復健環境',
    // content: '藉由收集個人基本的生理參數，經統計及資料分析後，推薦一個',
    // content2: '最適合自己的復健方式，提供使用者參考，並根據使用者過去的',
    // content3: '復健成效來判斷是否該加強復健的課程'
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className={style.block} >
        <Row className={style.header} >
          <div className={style.title}>
            {this.props.title}
          </div>
        </Row>
        <Row className={style.content} >
          <div style={{ display: 'block' }} >
            {this.props.content}
          </div>
          <div style={{ display: 'block' }} >
            {this.props.content2}
          </div>
          <div style={{ display: 'block' }} >
            {this.props.content3}
          </div>
          {/* <div style={{ display: 'block' }} >
            {this.state.content2}
          </div> */}
        </Row>
      </div>


    )
  }
}

export default (Index);

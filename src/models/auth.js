import {
  POST_LOGIN,
  POST_REGISTER,
  POST_CHANGEPASSWORD,
  GET_USERDATA,
  POST_EDITPORFILE,
  POST_FORGET,
  POST_RESET
} from '../services/auth';
import { routerRedux } from 'dva/router';
import _ from 'lodash';
import { message } from 'antd';

export default {

  //作為呼叫目前model的命名
  namespace: 'auth',

  //初始化state的狀態，
  state: {
    userData: null,
  },

  //表示訂閱數據源
  subscriptions: {

  },

  //處理非同步邏輯 在這裡串接api(yield call) 並且將回傳資料帶回reducers(yield put)
  effects: {
    *POST_LOGIN({ payload, callback, close }, { call, put }) {
      try {
        const response = yield call(POST_LOGIN, payload);
        localStorage.setItem('userInfo', JSON.stringify(response.token));
        const token = JSON.parse(localStorage.getItem('userInfo'))
        let userData = yield call(GET_USERDATA, token);
        userData = { ...userData, img: `/${userData.img}` };

        yield put({
          type: 'loginHandle', //指定type 來傳送資料reducers
          payload: {           //回傳值
            userData: userData,
          }
        })
        if (callback) { callback() }
        if (close) { close() }
        message.success('已成功登入');
        yield put(routerRedux.push('/dashboard/private'), window.location.reload());
      } catch (err) {
        if (err.response.status === 401) {
          message.error('帳號密碼錯誤');
        }
        console.log(err.response.status);
      }
    },
    *POST_REGISTER({ payload, callback, close }, { call, put }) {
      try {
        payload = _.omit(payload, ['passwordcheck']);
        const response = yield call(POST_REGISTER, payload);
        console.log(response.status);
        if (callback) { callback() }
        if (close) { close() }
        yield put(routerRedux.push('/member/success'));
        console.log(response);
      } catch (err) {
        if (err.response.status === 401) {
          message.error('此帳號已註冊過了');
        } else if (err.response.status === 403) {
          message.error('輸入格式驗證錯誤');
        }
      }
    },
    *GET_USERDATA({ payload }, { call, put }) {
      try {
        const token = JSON.parse(localStorage.getItem('userInfo'));
        let response = yield call(GET_USERDATA, token);
        //response = {...response, img: `/${response.img}`};
        yield put({
          type: 'userdataHandle',
          payload: {
            userData: response,
          }
        })
      } catch (err) {
        message.error('帳號資料錯誤');
      }
    },
    *POST_EDITPROFILE({ payload, callback }, { call, put }) {
      try {
        const token = JSON.parse(localStorage.getItem('userInfo'));
        payload = _.omit(payload, ['birthday', 'sex', 'member_id']);
        const response = yield call(POST_EDITPORFILE, token, payload);
        console.log(response);
        message.success('修改成功')
        if (callback) { callback() }
        const userData = yield call(GET_USERDATA, token);
        yield put({
          type: 'changeHandle',
          payload: {
            userData: userData,
          }
        })
      } catch (err) {
        if (err.response.status === 401) {
          message.error('修改失敗');
        }
        else if (err.response.status === 403) {
          message.error('輸入格式驗證錯誤');
        }
      }
    },
    *POST_CHANGEPASSWORD({ payload, callback }, { call, put }) {
      try {
        const token = JSON.parse(localStorage.getItem('userInfo'));
        payload = _.omit(payload, ['newpasswordcheck']);
        const response = yield call(POST_CHANGEPASSWORD, token, payload);
        console.log(response);
        message.success('修改密碼成功，請重新登入')
        if (callback) { callback() }
        localStorage.clear();
        yield put(routerRedux.push('/'), window.location.reload());
      } catch (err) {
        if (err.response.status === 401) {
          message.error('舊密碼輸入錯誤');
        } else if (err.response.status === 403) {
          message.error('格式驗證錯誤');
        }
      }
    },
    *POST_FORGET({ payload, callback, close }, { call, put }) {
      try {
        const response = yield call(POST_FORGET, payload);
        message.success('輸入成功，接下來請去接受驗證信')

        if (callback) { callback() }
        if (close) { close() }
      } catch (err) {
        if (err.response.status === 401) {
          message.error('查無此帳號');
        }
      }
    },
    *POST_RESET({ payload, callback, close }, { call, put }) {
      try {
        payload = _.omit(payload, ['passwordcheck']);
        console.log(payload);
        const response = yield call(POST_RESET, payload);
        if (callback) { callback() }
        if (close) { close() }
        message.success('修改成功，請登入帳號!');
        yield put(routerRedux.push('/'));
      } catch (err) {
        if (err.response.status === 401) {
          message.error('驗證碼輸入錯誤，請檢查驗證碼');
        }
      }
    }
  },


  //同步處理邏輯  用來存放資料到models的state
  reducers: {
    loginHandle(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    userdataHandle(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    changeHandle(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    }
  },

};

import {
  GET_DATAMARKER,
  GET_PARTFREQUENCY,
  GET_AGETFREQUENCY,
  GET_HISTORICALRECORD_5,
  POST_HISTORICALRECORD_TIME,
  GET_SEXFREQUENCY,
  GET_LEGFREQUENCY,
  GET_LASTEVALUATION
} from '../services/data';
import _ from 'lodash';
import { message } from 'antd';
import moment from 'moment'

export default {

  namespace: 'data',

  state: {
    dataMarker: null,
    // dataMarker: [{ id: 1, value: 302 },
    //   { id: 2, value: 318 },
    //   { id: 3, value: 303 },
    //   { id: 4, value: 312 },
    //   { id: 5, value: 309 },
    //   { id: 6, value: 304 },
    //   { id: 7, value: 316 },
    //   { id: 8, value: 300 },
    //   { id: 9, value: 317 },
    //   { id: 10, value: 302 },
    //   { id: 11, value: 311 },
    //   { id: 12, value: 309 },
    //   { id: 13, value: 303 },
    //   { id: 14, value: 316 },
    //   { id: 15, value: 300 },
    //   { id: 16, value: 316 },
    //   { id: 17, value: 302 },
    //   { id: 18, value: 310 },
    //   { id: 19, value: 309 },
    //   { id: 20, value: 303 },
    //   { id: 21, value: 315 },
    //   { id: 22, value: 299 },
    //   { id: 23, value: 316 },
    //   { id: 24, value: 302 },
    //   { id: 25, value: 310 },
    //   { id: 26, value: 312 },
    //   { id: 27, value: 302 },
    //   { id: 28, value: 319 },
    //   { id: 29, value: 301 },
    //   { id: 30, value: 318 },
    //   { id: 31, value: 306 },
    //   { id: 32, value: 317 },
    //   { id: 33, value: 313 },
    //   { id: 34, value: 304 },
    //   { id: 35, value: 317 },
    //   { id: 36, value: 302 },
    //   { id: 37, value: 308 },
    //   { id: 38, value: 299 },
    //   { id: 39, value: 305 },
    //   { id: 40, value: 314 },
    //   { id: 41, value: 313 },
    //   { id: 42, value: 332 },
    //   { id: 43, value: 318 },
    //   { id: 44, value: 327 },
    //   { id: 45, value: 310 },
    //   { id: 46, value: 314 },
    //   { id: 47, value: 314 },
    //   { id: 48, value: 304 },
    //   { id: 49, value: 322 },
    //   { id: 50, value: 303 },
    //   { id: 51, value: 304 },
    //   { id: 52, value: 301 },
    //   { id: 53, value: 303 },
    //   { id: 54, value: 294 },
    //   { id: 55, value: 293 },
    //   { id: 56, value: 312 },
    //   { id: 57, value: 286 },
    //   { id: 58, value: 302 },
    //   { id: 59, value: 303 },
    //   { id: 60, value: 313 },
    //   { id: 61, value: 320 },
    //   { id: 62, value: 330 },
    //   { id: 63, value: 351 },
    //   { id: 64, value: 347 },
    //   { id: 65, value: 328 },
    //   { id: 66, value: 322 },
    //   { id: 67, value: 317 },
    //   { id: 68, value: 303 },
    //   { id: 69, value: 298 },
    //   { id: 70, value: 307 },
    //   { id: 71, value: 293 },
    //   { id: 72, value: 302 },
    //   { id: 73, value: 295 },
    //   { id: 74, value: 301 },
    //   { id: 75, value: 306 },
    //   { id: 76, value: 299 },
    //   { id: 77, value: 316 },
    //   { id: 78, value: 300 },
    //   { id: 79, value: 313 },
    //   { id: 80, value: 301 },
    //   { id: 81, value: 297 },
    //   { id: 82, value: 295 },
    //   { id: 83, value: 287 },
    //   { id: 84, value: 304 },
    //   { id: 85, value: 292 },
    //   { id: 86, value: 307 },
    //   { id: 87, value: 300 },
    //   { id: 88, value: 297 },
    //   { id: 89, value: 303 },
    //   { id: 90, value: 285 },
    //   { id: 91, value: 306 },
    //   { id: 92, value: 281 },
    //   { id: 93, value: 295 },
    //   { id: 94, value: 283 },
    //   { id: 95, value: 286 },
    //   { id: 96, value: 300 },
    //   { id: 97, value: 290 },
    //   { id: 98, value: 311 },
    //   { id: 99, value: 297 },
    //   { id: 100, value: 314 },
    //   { id: 101, value: 309 },
    //   { id: 102, value: 305 },
    //   { id: 103, value: 320 },
    //   { id: 104, value: 302 },
    //   { id: 105, value: 321 },
    //   { id: 106, value: 306 },
    //   { id: 107, value: 322 },
    //   { id: 108, value: 314 },
    //   { id: 109, value: 308 },
    //   { id: 110, value: 323 },
    //   { id: 111, value: 311 },
    //   { id: 112, value: 328 },
    //   { id: 113, value: 315 },
    //   { id: 114, value: 332 },
    //   { id: 115, value: 344 },
    //   { id: 116, value: 334 },
    //   { id: 117, value: 330 },
    //   { id: 118, value: 309 },
    //   { id: 119, value: 314 },
    //   { id: 120, value: 297 },
    //   { id: 121, value: 295 },
    //   { id: 122, value: 295 },
    //   { id: 123, value: 294 },
    //   { id: 124, value: 302 },
    //   { id: 125, value: 289 },
    //   { id: 126, value: 320 },
    //   { id: 127, value: 302 },
    //   { id: 128, value: 316 },
    //   { id: 129, value: 318 },
    //   { id: 130, value: 310 },
    //   { id: 131, value: 311 },
    //   { id: 132, value: 303 },
    //   { id: 133, value: 320 },
    //   { id: 134, value: 303 },
    //   { id: 135, value: 306 },
    //   { id: 136, value: 306 },
    //   { id: 137, value: 292 },
    //   { id: 138, value: 322 },
    //   { id: 139, value: 306 },
    //   { id: 140, value: 318 },
    //   { id: 141, value: 317 },
    //   { id: 142, value: 332 },
    //   { id: 143, value: 352 },
    //   { id: 144, value: 351 },
    //   { id: 145, value: 397 },
    //   { id: 146, value: 374 },
    //   { id: 147, value: 368 },
    //   { id: 148, value: 329 },
    //   { id: 149, value: 293 },
    //   { id: 150, value: 282 },
    //   { id: 151, value: 263 },
    //   { id: 152, value: 273 },
    //   { id: 153, value: 264 },
    //   { id: 154, value: 281 },
    //   { id: 155, value: 281 },
    //   { id: 156, value: 286 },
    //   { id: 157, value: 293 },
    //   { id: 158, value: 273 },
    //   { id: 159, value: 287 },
    //   { id: 160, value: 265 },
    //   { id: 161, value: 279 },
    //   { id: 162, value: 272 },
    //   { id: 163, value: 278 },
    //   { id: 164, value: 300 },
    //   { id: 165, value: 292 },
    //   { id: 166, value: 314 },
    //   { id: 167, value: 298 },
    //   { id: 168, value: 314 },
    //   { id: 169, value: 308 },
    //   { id: 170, value: 308 },
    //   { id: 171, value: 317 },
    //   { id: 172, value: 299 },
    //   { id: 173, value: 316 },
    //   { id: 174, value: 298 },
    //   { id: 175, value: 310 },
    //   { id: 176, value: 307 },
    //   { id: 177, value: 302 },
    //   { id: 178, value: 314 },
    //   { id: 179, value: 301 },
    //   { id: 180, value: 323 },
    //   { id: 181, value: 303 },
    //   { id: 182, value: 319 },
    //   { id: 183, value: 321 },
    //   { id: 184, value: 310 },
    //   { id: 185, value: 320 },
    //   { id: 186, value: 302 },
    //   { id: 187, value: 308 },
    //   { id: 188, value: 290 },
    //   { id: 189, value: 293 },
    //   { id: 190, value: 294 },
    //   { id: 191, value: 288 },
    //   { id: 192, value: 303 },
    //   { id: 193, value: 294 },
    //   { id: 194, value: 323 },
    //   { id: 195, value: 326 },
    //   { id: 196, value: 337 },
    //   { id: 197, value: 335 },
    //   { id: 198, value: 320 },
    //   { id: 199, value: 332 },
    //   { id: 200, value: 320 },
    //   { id: 201, value: 344 },
    //   { id: 202, value: 334 },
    //   { id: 203, value: 337 },
    //   { id: 204, value: 343 },
    //   { id: 205, value: 347 },
    //   { id: 206, value: 371 },
    //   { id: 207, value: 364 },
    //   { id: 208, value: 364 },
    //   { id: 209, value: 364 },
    //   { id: 210, value: 361 },
    //   { id: 211, value: 355 },
    //   { id: 212, value: 331 },
    //   { id: 213, value: 356 },
    //   { id: 214, value: 335 },
    //   { id: 215, value: 359 },
    //   { id: 216, value: 355 },
    //   { id: 217, value: 370 },
    //   { id: 218, value: 386 },
    //   { id: 219, value: 356 },
    //   { id: 220, value: 351 },
    //   { id: 221, value: 308 },
    //   { id: 222, value: 296 },
    //   { id: 223, value: 273 },
    //   { id: 224, value: 258 },
    //   { id: 225, value: 256 },
    //   { id: 226, value: 239 },
    //   { id: 227, value: 247 },
    //   { id: 228, value: 230 },
    //   { id: 229, value: 238 },
    //   { id: 230, value: 238 },
    //   { id: 231, value: 236 },
    //   { id: 232, value: 251 },
    //   { id: 233, value: 236 },
    //   { id: 234, value: 255 },
    //   { id: 235, value: 239 },
    //   { id: 236, value: 251 },
    //   { id: 237, value: 256 },
    //   { id: 238, value: 255 },
    //   { id: 239, value: 274 },
    //   { id: 240, value: 261 },
    //   { id: 241, value: 290 },
    //   { id: 242, value: 292 },
    //   { id: 243, value: 305 },
    //   { id: 244, value: 291 },
    //   { id: 245, value: 270 },
    //   { id: 246, value: 281 },
    //   { id: 247, value: 265 },
    //   { id: 248, value: 287 },
    //   { id: 249, value: 279 },
    //   { id: 250, value: 290 },
    //   { id: 251, value: 303 },
    //   { id: 252, value: 293 },
    //   { id: 253, value: 312 },
    //   { id: 254, value: 305 },
    //   { id: 255, value: 325 },
    //   { id: 256, value: 317 },
    //   { id: 257, value: 326 },
    //   { id: 258, value: 335 },
    //   { id: 259, value: 332 },
    //   { id: 260, value: 353 },
    //   { id: 261, value: 339 },
    //   { id: 262, value: 354 },
    //   { id: 263, value: 342 },
    //   { id: 264, value: 332 },
    //   { id: 265, value: 331 },
    //   { id: 266, value: 307 },
    //   { id: 267, value: 318 },
    //   { id: 268, value: 296 },
    //   { id: 269, value: 309 },
    //   { id: 270, value: 314 },
    //   { id: 271, value: 331 },
    //   { id: 272, value: 330 },
    //   { id: 273, value: 320 },
    //   { id: 274, value: 324 },
    //   { id: 275, value: 310 },
    //   { id: 276, value: 333 },
    //   { id: 277, value: 333 },
    //   { id: 278, value: 334 },
    //   { id: 279, value: 357 },
    //   { id: 280, value: 338 },
    //   { id: 281, value: 361 },
    //   { id: 282, value: 359 },
    //   { id: 283, value: 377 },
    //   { id: 284, value: 356 },
    //   { id: 285, value: 370 },
    //   { id: 286, value: 361 },
    //   { id: 287, value: 355 },
    //   { id: 288, value: 353 },
    //   { id: 289, value: 328 },
    //   { id: 290, value: 315 },
    //   { id: 291, value: 301 },
    //   { id: 292, value: 278 },
    //   { id: 293, value: 277 },
    //   { id: 294, value: 250 },
    //   { id: 295, value: 253 },
    //   { id: 296, value: 243 },
    //   { id: 297, value: 249 },
    //   { id: 298, value: 255 },
    //   { id: 299, value: 249 },
    //   { id: 300, value: 270 },
    //   { id: 301, value: 259 },
    //   { id: 302, value: 283 },
    //   { id: 303, value: 277 },
    //   { id: 304, value: 280 },
    //   { id: 305, value: 288 },
    //   { id: 306, value: 279 },
    //   { id: 307, value: 293 },
    //   { id: 308, value: 271 },
    //   { id: 309, value: 281 },
    //   { id: 310, value: 270 },
    //   { id: 311, value: 275 },
    //   { id: 312, value: 291 },
    //   { id: 313, value: 280 },
    //   { id: 314, value: 299 },
    //   { id: 315, value: 286 },
    //   { id: 316, value: 300 },
    //   { id: 317, value: 296 },
    //   { id: 318, value: 295 },
    //   { id: 319, value: 309 },
    //   { id: 320, value: 294 },
    //   { id: 321, value: 310 },
    //   { id: 322, value: 300 },
    //   { id: 323, value: 315 },
    //   { id: 324, value: 315 },
    //   { id: 325, value: 313 },
    //   { id: 326, value: 336 },
    //   { id: 327, value: 332 },
    //   { id: 328, value: 353 },
    //   { id: 329, value: 346 },
    //   { id: 330, value: 347 },
    //   { id: 331, value: 336 },
    //   { id: 332, value: 325 },
    //   { id: 333, value: 327 },
    //   { id: 334, value: 308 },
    //   { id: 335, value: 321 },
    //   { id: 336, value: 310 },
    //   { id: 337, value: 315 },
    //   { id: 338, value: 319 },
    //   { id: 339, value: 319 },
    //   { id: 340, value: 322 },
    //   { id: 341, value: 343 },
    //   { id: 342, value: 389 },
    //   { id: 343, value: 433 },
    //   { id: 344, value: 468 },
    //   { id: 345, value: 517 },
    //   { id: 346, value: 499 },
    //   { id: 347, value: 522 },
    //   { id: 348, value: 490 },
    //   { id: 349, value: 455 },
    //   { id: 350, value: 408 },
    //   { id: 351, value: 353 },
    //   { id: 352, value: 315 },
    //   { id: 353, value: 263 },
    //   { id: 354, value: 239 },
    //   { id: 355, value: 199 },
    //   { id: 356, value: 189 },
    //   { id: 357, value: 173 },
    //   { id: 358, value: 166 },
    //   { id: 359, value: 180 },
    //   { id: 360, value: 169 },
    //   { id: 361, value: 195 },
    //   { id: 362, value: 189 },
    //   { id: 363, value: 208 },
    //   { id: 364, value: 214 },
    //   { id: 365, value: 212 },
    //   { id: 366, value: 234 },
    //   { id: 367, value: 222 },
    //   { id: 368, value: 248 },
    //   { id: 369, value: 239 },
    //   { id: 370, value: 251 },
    //   { id: 371, value: 259 },
    //   { id: 372, value: 256 },
    //   { id: 373, value: 277 },
    //   { id: 374, value: 266 },
    //   { id: 375, value: 290 },
    //   { id: 376, value: 282 },
    //   { id: 377, value: 287 },
    //   { id: 378, value: 296 },
    //   { id: 379, value: 290 },
    //   { id: 380, value: 306 },
    //   { id: 381, value: 294 },
    //   { id: 382, value: 309 },
    //   { id: 383, value: 303 },
    //   { id: 384, value: 307 },
    //   { id: 385, value: 309 },
    //   { id: 386, value: 294 },
    //   { id: 387, value: 309 },
    //   { id: 388, value: 287 },
    //   { id: 389, value: 310 },
    //   { id: 390, value: 293 },
    //   { id: 391, value: 292 },
    //   { id: 392, value: 302 },
    //   { id: 393, value: 296 },
    //   { id: 394, value: 323 },
    //   { id: 395, value: 287 },
    //   { id: 396, value: 319 },
    //   { id: 397, value: 321 },
    //   { id: 398, value: 325 },
    //   { id: 399, value: 342 }]

  },

  effects: {
    *GET_DATAMARKER({ payload }, { call, put }) {
      try {
        const dataMarker = yield call(GET_DATAMARKER, payload);
        console.log(dataMarker);
        _.map(dataMarker, item => {
          item.date = moment(item.date).format('YYYY-MM-DD HH:mm');
        })
        yield put({
          type: 'getEMGHandle',
          payload: {
            dataMarker: dataMarker,
          }
        })
      } catch (err) {
        if (err.response.status === 401) {
          message.error('未取得帳號');
        }
        console.log(err.response.status);
      }

    },
    *GET_PARTFREQUENCY({ payload }, { call, put }) {
      const response = yield call(GET_PARTFREQUENCY, payload);
      yield put({
        type: 'getPartFrequency',
        payload: {
          PartFrequency: response,
        }
      })
    },
    *GET_LEGFREQUENCY({ payload }, { call, put }) {
      const response = yield call(GET_LEGFREQUENCY, payload);
      yield put({
        type: 'getLegFrequency',
        payload: {
          LegFrequency: response,
        }
      })
    },
    *GET_AGEFREQUENCY({ payload }, { call, put }) {
      const response = yield call(GET_AGETFREQUENCY, payload);

      yield put({
        type: 'getAgeFrequency',
        payload: {
          AgeFrequency: response,
        }
      })
    },
    *GET_HISTORICALRECORD_5({ payload }, { call, put }) {
      try {
        const response = yield call(GET_HISTORICALRECORD_5, payload);
        _.map(response, item => {
          item.date = moment(item.date).format('YYYY-MM-DD');
        })
        console.log(response);
        yield put({
          type: 'getHistoricalRecord',
          payload: {
            HistoricalRecord: response,
          }
        })
      }
      catch (err) {
        // if (err.response.status === 401) {
        //   message.error('資料取得失敗');
        // }
        console.log(err.response.status);
      }
    },
    *POST_HISTORICALRECORD_TIME({ payload }, { call, put }) {
      try {
        const response = yield call(POST_HISTORICALRECORD_TIME, payload);
        _.map(response, item => {
          item.date = moment(item.date).format('YYYY-MM-DD')
        })
        yield put({
          type: 'getHistoricalRecord',
          payload: {
            HistoricalRecord: response,
          }
        })
      }
      catch (err) {

      }
    },
    *GET_SEXFREQUENCY({ payload }, { call, put }) {
      try {
        const response = yield call(GET_SEXFREQUENCY, payload);

        yield put({
          type: 'getSexFrequency',
          payload: {
            SexFrequency: response,
          }
        })
      }
      catch (err) {
        if (err.response.status === 401) {
          message.error('資料取得失敗');
        }
      }
    },
    *GET_LASTEVALUATION({ payload }, { call, put }){
      try {
        const response = yield call(GET_LASTEVALUATION, payload);
        console.log(response);
        yield put({
          type: 'getLastEvaluation',
          payload: {
            LastEvaluation: response,
          }
        })
      }catch(err){
        if (err.response.status === 401) {
          message.error('未取得帳號');
        }
        console.log(err.response.status);
      }
    },

  },

  reducers: {
    getEMGHandle(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getPartFrequency(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getAgeFrequency(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getHistoricalRecord(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getSexFrequency(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getLegFrequency(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getLastEvaluation(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  }
}

import request from '../utils/request';

// auth.login
export function POST_LOGIN(payload) {
  return request.post('/api/member/login', payload, {});
}

export function POST_FORGET(payload) {
  return request.post('/api/member/forgetpassword', payload, {});
}

export function POST_RESET(payload) {
  return request.post('/api/member/resetpassword', payload, {});
}

export function POST_REGISTER(payload) {
  return request.post('/api/member/register', payload, {});
}

export function GET_USERDATA(token) {
  return request.get('/api/member/userinfo', {}, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function POST_EDITPORFILE(token,payload){
  return request.post('/api/member/changeother', payload, {
    headers: {
      Authorization: `Bearer ${token}`
    },
  });
}

export function POST_CHANGEPASSWORD(token,payload){
  return request.post('/api/member/changepassword', payload, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

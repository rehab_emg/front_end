import request from '../utils/request';

export function GET_DATAMARKER(payload) {
  console.log(payload);
  return request.get('/api/emg/lastrecord', {}, {
    headers: {
      account: payload,
    },
  });
}

export function GET_PARTFREQUENCY() {
  return request.get('/api/emg/parttimes', {}, {});
}

export function GET_LEGFREQUENCY() {
  return request.get('/api/emg/legtimes', {}, {});
}

export function GET_AGETFREQUENCY() {
  return request.get('/api/emg/agetimes', {}, {});
}

export function GET_HISTORICALRECORD_5(payload) {
  return request.get('/api/emg/hisrecord5', {}, {
    headers: {
      account: payload,
    },
  });
}

export function POST_HISTORICALRECORD_TIME(payload) {
  return request.post('/api/emg/hisrecordtime', payload, {});
}

export function GET_LASTEVALUATION(payload){
  return request.get('/api/emg/lastevaluation', {}, {
    headers: {
      account: payload,
    },
  });
}

export function GET_SEXFREQUENCY(){
  return request.get('/api/emg/sextimes', {}, {});
}

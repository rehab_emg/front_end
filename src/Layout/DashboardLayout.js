import React, { Component } from 'react';
import { routerRedux } from 'dva/router';
import { Button, Layout, Menu, Icon, Col, Row, Modal, Avatar, message, Breadcrumb } from 'antd';
import style from './DashboardLayout.css';
import { connect } from 'dva';
import apiConfig from '../config';
import _ from 'lodash';
const { Header, Sider, Content } = Layout;
const SubMenu = Menu.SubMenu;



class DashboardLayout extends Component {
  state = {
    collapsed: false,
    loading: false,
    visible: false,
    width:0
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });

  }

  componentDidMount = () => {
    const value = JSON.parse(localStorage.getItem('userInfo'));
    window.addEventListener('resize', this.handleResize);
    this.setState({ width: window.innerWidth});
    _.isEmpty(value) ? (this.props.dispatch(routerRedux.push('/member/notlogin'))) : this.setRenderComponent(this.props)

  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    this.setState({ width: window.innerWidth});
    if(this.state.width < 700){
      this.setState({
        collapsed: true,
      });
    }
    else{
      this.setState({
        collapsed: false,
      });
    }
  }

  setRenderComponent() {
    const { dispatch } = this.props;
    const value = JSON.parse(localStorage.getItem('userInfo'));
    dispatch({
      type: 'auth/GET_USERDATA',
      payload: value,
    })
    // _.isNull(localStorage.getItem('userInfo')) ? dispatch({ type: 'auth/GET_USERDATA' }) : console.log(userData)
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      localStorage.clear();
      this.props.dispatch(routerRedux.push('/'));
      message.success('已成功登出');
      this.setState({ loading: false, visible: false });
    }, 1500);
  }

  handleCancel = () => {
    this.setState({ visible: false });
  }

  handleLogout = (e) => {
    this.setState({
      key: [e],
    })
  }

  handleMenuClick = (e) => {
    if (e.key === '/') {
      this.showModal()
    }
    else {
      this.setState({
        key: [e],
      })
      this.props.dispatch(routerRedux.push(e.key));
    }
  }

  backtoIndex = () => {
    this.props.dispatch(routerRedux.push('/'));
  }

  render() {
    const { userData } = this.props;
    const { visible, loading } = this.state;
    console.log(userData);
    console.log(this.props);
    return (
      _.isNull(userData) ?
        null :
        <Layout style={{ height: '100%' }}>
          <Sider
            breakpoint="lg"
            trigger={null}
            width="300"
            collapsed={this.state.collapsed}
            className={style.main_color}
          >
            <div className={style.logo} id="logo" >
              <img src="/src/assets/REHAB-LOGO.png" alt="" onClick={this.backtoIndex} />
              <h2 className={style.function_title} style={{ display: 'inlineBlock' }}>EMG ReHab</h2>
            </div>
            <Menu onClick={(item) => this.handleMenuClick(item)} className={style.main_color} theme="dark" mode="inline" defaultSelectedKeys={['private']}>
              <Menu.Item style={{ padding: "10px 0 50px 24px" }} key="/dashboard/private">
                <Icon type="user" />
                <span>個人狀況</span>
              </Menu.Item>
              <Menu.Item style={{ padding: "10px 0 50px 24px" }} key="/dashboard/public">
                <Icon type="pie-chart" />
                <span>數據分析</span>
              </Menu.Item>
              <SubMenu
                style={{ padding: "10px 0 25px 0px" }}
                key="sub1"
                title={<span><Icon type="star-o" /><span>會員專區</span></span>}
              >
                <Menu.Item style={{ padding: "10px 0 50px 24px" }} key="/dashboard/profile">
                  <Icon type="contacts" />
                  <span>會員資料</span>
                </Menu.Item>
                <Menu.Item style={{ padding: "10px 0 50px 24px" }} key="/dashboard/changepassword">
                  <Icon type="lock" />
                  <span>變更密碼</span>
                </Menu.Item>
              </SubMenu>
              <Menu.Item style={{ padding: "10px 0 50px 24px" }} key="/" >
                <Icon type="logout" />
                <span>登出</span>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Header style={{ background: '#fff', padding: '2px 0', }}>
              <Row>
                <Col span={4}>
                  <Icon
                    className={style.trigger}
                    type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                    onClick={this.toggle}
                  />
                </Col>
                <Col xs={17} lg={14} xl={17}></Col>
                <Col xs={3} lg={6} xl={3} >
                  {
                    _.isNull(userData.img) ? <Avatar size="large" icon="user" /> : <Avatar size="large" src={`${apiConfig.api}/${userData.img}`} />
                  }
                  <span className={style.name} media="screen and (max-width: 1004px)" style={{ margin: '0 10px', 'fontSize': '16px', textAlign: 'right' }}>{userData.name}</span>
                </Col>
              </Row>
            </Header>

            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: window.width }}>
              {
                this.props.children
              }
            </Content>
          </Layout>
          <Modal
            visible={visible}
            title="確定要登出嗎?"
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={[
              <Button key="back" onClick={this.handleCancel}>取消</Button>,
              <Button key="submit" type="danger" loading={loading} onClick={this.handleOk}>
                是
              </Button>,
            ]}
          >
            <p>登出後將回到首頁</p>
          </Modal>
        </Layout>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: _.get(state, 'auth.isLogin', null),
    userData: _.get(state, 'auth.userData', null),
  }
}

export default connect(mapStateToProps)(DashboardLayout);

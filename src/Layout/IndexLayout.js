import React, { Component } from 'react';
import { routerRedux } from 'dva/router';
import { Layout, Spin, Row, Col, Button, Modal, message, Icon, Dropdown, Menu } from 'antd';
import style from './IndexLayout.css';
import LoginForm from '../component/form/Login';
import RegisterForm from '../component/form/Register';
import ForgetPassForm from '../component/form/ForgetPassword';
import { connect } from 'dva';
import _ from 'lodash';
const { Header, Footer, Content } = Layout;



class IndexLayout extends Component {
  state = {
    loading: false,
    Loginvisible: false,
    RegisterVisible: false,
    ForgetPassVisible: false,
    auth: {
      isLogin: false,
      userInfo: [],
    },
    width: 0,
    height: 0
  }

  showLoginModal = () => {
    this.setState({
      LoginVisible: true,
    });
  }

  underLoginshowRegisterModal = () => {
    this.setState({
      LoginVisible: false,
      RegisterVisible: true,
    })
  }

  underLoginshowForgetPassModal = () => {
    this.setState({
      LoginVisible: false,
      ForgetPassVisible: true,
    })
  }

  showRegisterModal = () => {
    this.setState({
      RegisterVisible: true,
    })
  }

  showForgetPassModal = () => {
    this.setState({
      ForgetPassVisible: true,
    })
  }

  handleCancel = (e) => {
    this.setState({
      LoginVisible: false,
      RegisterVisible: false,
      ForgetPassVisible: false,
    });
  }

  handleLogin = (value, callback) => {
    const { dispatch } = this.props;
    this.setState({
      loading: true,
    })
    dispatch({
      type: 'auth/POST_LOGIN',
      payload: value,
      callback,
      close: this.handleCancel(),
    }, () => {
      this.setState({
        loading: false,
      })
    })
  }

  handleForgetSubmit = (value, callback) => {
    const { dispatch } = this.props;

    dispatch({
      type: 'auth/POST_FORGET',
      payload: value,
      callback,
      close: this.handleCancel(),
    })
  }

  UNSAFE_componentWillReceiveProps = (nextProps) => {
    console.log(this.props);
    console.log(nextProps.width);
    if (this.props.width !== nextProps.width) {
      this.setState = ({
        width: nextProps.width
      });
    }
  }

  componentDidMount = () => {
    const { dispatch } = this.props;
    const value = JSON.parse(localStorage.getItem('userInfo'));
    // this.updateWindowDimensions();
    window.addEventListener('resize', this.handleResize);
    this.setState({ width: window.innerWidth });
    _.isNull(value) ? console.log('notlogin') : dispatch({
      type: 'auth/GET_USERDATA',
      payload: value,
    })
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    this.setState({ width: window.innerWidth });
  }

  handleRegister = (value, callback) => {
    const { dispatch } = this.props;
    this.setState({
      loading: true,
    })
    dispatch({
      type: 'auth/POST_REGISTER',
      payload: value,
      callback,
      close: this.handleCancel(),
    }, () => {
      this.setState({
        loading: false,
      })
    })

  }

  handleLogout = () => {
    localStorage.clear();
    message.success('已成功登出');
    window.location.reload();
  }

  handleDashboard = () => {
    this.props.dispatch(routerRedux.push('/dashboard/private'));
  }



  // logo = props =>(
  //   <Icon component={this.loadingSvg} {...props} />
  // );

  render() {
    const value = JSON.parse(localStorage.getItem('userInfo'));
    //const loadingSvg = <img src="../assets/REHAB-LOGO.png" alt="" />;
    const logo = <Icon type="loading" style={{
      textAlign: 'center', fontSize: 100
      , position: 'fixed'
      , width: '100%'
      , top: '40%'
      , left: 0
      , margin: 'auto'
    }} spin />;

    const Loginmenu = (
      <Menu>
        <Menu.Item>
          <a target="_blank" onClick={this.handleDashboard}  >前往數據管理</a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" onClick={this.handleLogout}  >登出</a>
        </Menu.Item>
      </Menu>
    );
    const notLoginmenu = (
      <Menu>
        <Menu.Item>
          <a target="_blank" onClick={this.showLoginModal}  >登入</a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank" onClick={this.showRegisterModal}  >註冊</a>
        </Menu.Item>
      </Menu>
    );
    //const logo = <Icon component={loadingSvg} spin {...this.props} />
    return (

      <div>
        <Spin tip="載入中...." className="spin" spinning={this.state.loading} style={{ fontSize: '30px' }} indicator={logo}>
          <Layout className="layout">
            <Header style={{ position: "fixed", width: '100%', height: '80px', backgroundColor: 'rgba(0, 26, 48, 0.5)', padding: '0', 'zIndex': '999' }} >
              <Row gutter={24}>
                <Col span={5} offset={19} >
                  {
                    // !_.isNull(value) ? (
                    //   <div>
                    //     <Button size="large" className={style.dashBtn} onClick={this.handleDashboard} >前往數據管理</Button>
                    //     <Button size="large" className={style.loginBtn} onClick={this.handleLogout} >登出</Button>
                    //   </div>
                    // ) : (
                    //     <div>
                    //       <Button size="large" className={style.loginBtn} onClick={this.showLoginModal} >登入</Button>
                    //       <Button type="danger" size="large" className={style.loginBtn} onClick={this.showRegisterModal} >註冊</Button>
                    //     </div>
                    //   )
                  }
                  {

                    !_.isNull(value) ?
                      _.gte(this.state.width, 1214) ?
                        (
                          <div>
                            <Button size="large" className={style.dashBtn} onClick={this.handleDashboard} >前往數據管理</Button>
                            <Button size="large" className={style.loginBtn} onClick={this.handleLogout} >登出</Button>
                          </div>
                        ) :
                        (
                          <div style={{ textAlign: 'center' }}>
                            <Dropdown overlay={Loginmenu} placement="bottomCenter">
                              <Button className={style.menuBtn} icon="bars"></Button>
                            </Dropdown>
                          </div>
                        )
                      :
                      _.gte(this.state.width, 1214) ?
                        (
                          <div>
                            <Button size="large" className={style.loginBtn} onClick={this.showLoginModal} >登入</Button>
                            <Button type="danger" size="large" className={style.loginBtn} onClick={this.showRegisterModal} >註冊</Button>
                          </div>
                        )
                        :
                        (
                          <div style={{ textAlign: 'center' }}>
                            <Dropdown overlay={notLoginmenu} placement="bottomCenter">
                              <Button className={style.notLoginmenuBtn}><Icon type="user" theme="outlined" /></Button>
                            </Dropdown>
                          </div>
                        )
                  }
                </Col>
              </Row>
            </Header>
            <Content style={{ padding: '0' }}>
              {
                this.props.children
              }
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              <Row>
                <div className={style.mb}>
                  {
                    !_.isNull(value) ? (
                      <Button type="primary" target="_blank" href="https://drive.google.com/file/d/1SgepfgThZ5S5gpeLpTfSt9PDBb_TeZM9/view" style={{ 'fontSize': '20px', color: '#444343', background: '#DBE0E8', width: '300px', height: '100px', margin: '0 10px' }}>下載APP</Button>) :

                      <div>
                        <Button type="primary" style={{ 'fontSize': '20px', background: '#172E42', width: '300px', height: '100px', margin: '0 10px' }} onClick={this.showRegisterModal}>註冊</Button>
                        <Button type="primary" style={{ 'fontSize': '20px', color: '#444343', background: '#DBE0E8', width: '300px', height: '100px', margin: '0 10px' }}>下載APP</Button>
                      </div>
                  }
                </div>
              </Row>
              REHAB EMG ©2018 Created by JOJOWOWSENLAB
          </Footer>
          </Layout>
        </Spin>
        <Modal
          visible={this.state.LoginVisible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <LoginForm handleSubmit={this.handleLogin} underLoginshowRegisterModal={this.underLoginshowRegisterModal} underLoginshowForgetPassModal={this.underLoginshowForgetPassModal}></LoginForm>
        </Modal>
        <Modal
          visible={this.state.RegisterVisible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <RegisterForm handleSubmit={this.handleRegister}></RegisterForm>
        </Modal>
        <Modal
          visible={this.state.ForgetPassVisible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <ForgetPassForm handleForgetSubmit={this.handleForgetSubmit}></ForgetPassForm>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: _.get(state, 'auth.isLogin', null),
    userData: _.get(state, 'auth.userData', null),
  }
}

export default connect(mapStateToProps)(IndexLayout);

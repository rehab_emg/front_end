import React, { Component } from 'react';
import { connect } from 'dva';

class MemberLayout extends Component {
  render() {
    return (
      <div>
        <div>
          <img src="/src/assets/index.png" style={{ width: '100%', opacity: '0.5', zIndex: '-999', position: 'fixed' }} alt="" />
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default connect()(MemberLayout);

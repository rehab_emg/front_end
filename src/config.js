import _ from 'lodash';

const apiConfig = {
  protocol: _.get(window.env, 'API_PROTOCOL', false) || process.env.API_PROTOCOL || 'http',
  host: _.get(window.env, 'API_HOST', false) || process.env.API_HOST || 'localhost',
  port: _.get(window.env,'API_PORT',false) || process.env.API_PORT || 3000,
  prefix: _.get(window.env,'API_PREFIX', false) || process.env.API_PREFIX || '',
}

export default {
  api: `${apiConfig.protocol}://${apiConfig.host}:${apiConfig.port}/${apiConfig.prefix}`,
};

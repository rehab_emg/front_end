import React, { Component } from 'react';
import { Layout, Form } from 'antd';
import style from './ChangePassword.css';
import ChangePasswordForm from '../../../component/form/ChangePassword';
import { connect } from 'dva';
import _ from 'lodash';

const { Header, Footer, Sider, Content, UserForm } = Layout;

class ChangePassword extends Component {

  componentDidMount() {
    this.setRenderComponent(this.props);
  }

  setRenderComponent() {
    const { dispatch } = this.props;
    dispatch({ type: 'auth/GET_USERDATA' })

    // _.isNull(localStorage.getItem('userInfo')) ? dispatch({ type: 'auth/GET_USERDATA' }) : console.log(userData)
  }



  handleChange = (value) => {
    const { dispatch } = this.props;
    const callback = () => {
      this.setState({
       isloading: false,
      })
    }
    this.setState({
      isloading: true,
    }, () => {
      dispatch({
        type: 'auth/POST_CHANGEPASSWORD',
        payload: value,
        callback,
      })
    })

  }

  render() {
    const { userData } = this.props;
    return(
      _.isNull(userData) ? null :
      <div  style={{ minHeight: '815px' }} >
        <Header style={{ background: '#fff', padding: 0, margin: 0, fontSize: '30px', minHeight: 100, marginTop:'10px', marginBottom:'-30px' }}>
          修改密碼
        </Header>
        <ChangePasswordForm userData={userData} handleChange={this.handleChange} ></ChangePasswordForm>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: _.get(state, 'auth.isLogin', null),
    userData: _.get(state, 'auth.userData', null),
  }
}

export default connect(mapStateToProps)(ChangePassword);

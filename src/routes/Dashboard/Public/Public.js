import React, { Component } from 'react';
import { Card, Row, Col, Spin, Breadcrumb, Icon, Button } from 'antd';
import style from './Public.css';
import CardPiechart from '../../../component/card/chart/PieChart';
import CardBasicBarChart from '../../../component/card/chart/BasicBarChart';
import CardPieCustom from '../../../component/card/chart/PieCustom';
import CardBasicSingleBarChart from '../../../component/card/chart/BasicSingleBarChart';
import { connect } from 'dva';
import _ from 'lodash';

class Public extends Component {


  state={
    visible: true,
  }

  componentDidMount = () => {
    const { dispatch } = this.props;

    dispatch({
      type: 'data/GET_PARTFREQUENCY'
    })
    dispatch({
      type: 'data/GET_AGEFREQUENCY'
    })
    dispatch({
      type: 'data/GET_SEXFREQUENCY'
    })
    dispatch({
      type: 'data/GET_LEGFREQUENCY'
    })
  }

  handleChange = () => {
    this.setState({
      visible: false,
    });
  }

  handleBack = () => {
    this.setState({
      visible: true,
    });
  }

  render() {
    const { PartFrequency, AgeFrequency, SexFrequency, LegFrequency } = this.props;
    const loading_icon = <div style={{
      width: '100px',
      height: '100px',
      backgroundImage: 'url("https://i.imgur.com/t1q8b9P.gif")',
      backgroundSize: '100%'
    }} ></div>;
    console.log(LegFrequency);
    return (
      _.isNull(PartFrequency && AgeFrequency && SexFrequency && LegFrequency) ?
        <div className={style.loading}>
          <Spin tip="Loading..." indicator={loading_icon} />
        </div>
        :
        <div>
          <Breadcrumb>
            <Breadcrumb.Item>數據管理</Breadcrumb.Item>
            <Breadcrumb.Item>數據分析</Breadcrumb.Item>
          </Breadcrumb>
          <div>
            <Row gutter={40}>
              <Col className="gutter-row" lg={12} md={24}>
                <Card title={<div><Icon type="pie-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />註冊會員統計</div>} bordered={false}><CardPiechart AgeFrequency={AgeFrequency}></CardPiechart></Card>
              </Col>
              <Col className="gutter-row" lg={12} md={24}>
                {
                  this.state.visible === true ?
                    <Card title={<div><Icon type="trophy" theme="filled" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />最多檢測部位</div>} extra={<Button onClick={this.handleChange} style={{ backgroundColor: '#EDAE49', borderColor: '#EDAE49', color: '#ffffff' }}><Icon type="swap" style={{ fontSize: '20px' }}/></Button>} bordered={false}><CardBasicBarChart PartFrequency={PartFrequency} ></CardBasicBarChart></Card>
                    :
                    <Card title={<div><Icon type="trophy" theme="filled" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />最多檢測左右腳</div>} extra={<Button onClick={this.handleBack} style={{ backgroundColor: '#EDAE49', borderColor: '#EDAE49', color: '#ffffff' }}><Icon type="swap" style={{ fontSize: '20px' }}/></Button>} bordered={false}><CardBasicSingleBarChart LegFrequency={LegFrequency} ></CardBasicSingleBarChart></Card>
                }

              </Col>
            </Row>
            <Row gutter={40}>
              <Col className="gutter-row" lg={12} md={24}>
                <Card title={<div><Icon type="user" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />性別比</div>} bordered={false}><CardPieCustom SexFrequency={SexFrequency}></CardPieCustom></Card>
              </Col>
            </Row>
          </div>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    PartFrequency: _.get(state, 'data.PartFrequency', null),
    LegFrequency: _.get(state, 'data.LegFrequency', null),
    AgeFrequency: _.get(state, 'data.AgeFrequency', null),
    SexFrequency: _.get(state, 'data.SexFrequency', null),
  }
}

export default connect(mapStateToProps)(Public);


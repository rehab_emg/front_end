import React, { Component } from 'react';
import { routerRedux } from 'dva/router';
import { Card, Row, Col, Spin, List, Avatar, Breadcrumb, Button, Modal, Icon, Progress } from 'antd';
import style from './Private.css';
import CardDataMarker from '../../../component/card/chart/DataMarker';
import RangePickerForm from '../../../component/form/DateRangePicker';
import ListDrawer from '../../../component/drawer/ListDrawer';
import BasicArea from '../../../component/card/chart/BasicAreaChart';
import CardSeriesChart from '../../../component/card/chart/SeriesChart';
import CardBasicAnalysisLineChart from '../../../component/card/chart/BasicAnalysisLineChart';
import { connect } from 'dva';
import apiConfig from '../../../config';
import moment from 'moment';
import _ from 'lodash';
import G2 from '@antv/g2';
const { Meta } = Card;

class Private extends Component {

  state = {
    pStyle: {
      fontSize: 16,
      color: 'rgba(0,0,0,0.85)',
      lineHeight: '24px',
      display: 'block',
      verticalAlign: 'middle',
      marginBottom: 16,
    },
    list: [
      // {
      //   name: '2018-09-28',
      //   content: '左腳 股外側肌'
      // },
      // {
      //   name: '2018-09-10',
      //   content: '右腳 股外側肌'
      // },
      // {
      //   name: '2018-09-01',
      //   content: '右腳 股外側肌'
      // },
      // {
      //   name: '2018-08-30',
      //   content: '右腳 股外側肌'
      // },
    ],
    data: [],
    Comparevisible: false,
  }

  componentDidMount() {
    const value = JSON.parse(localStorage.getItem('userInfo'));
    _.isEmpty(value) ? (this.props.dispatch(routerRedux.push('/member/notlogin'))) : (this.setRenderComponent(this.props))
  }

  setRenderComponent() {
    const { dispatch, userData } = this.props;
    dispatch({ type: 'auth/GET_USERDATA' });
    const value = userData.account;
    dispatch({
      type: 'data/GET_DATAMARKER',
      payload: value,
    })
    dispatch({
      type: 'data/GET_LASTEVALUATION',
      payload: value,
    })
    dispatch({
      type: 'data/GET_HISTORICALRECORD_5',
      payload: value,
    })
    // _.isNull(localStorage.getItem('userInfo')) ? dispatch({ type: 'auth/GET_USERDATA' }) : console.log(userData)
  }

  showDrawer = (item) => {
    const { userData } = this.props;
    const record = { ...item, name: userData.name, img: userData.img };
    this.setState({
      record: record,
      visible: true,
    });
  };

  onClose = (value) => {
    this.setState({
      visible: value,
    });
  };

  handleChange = (value) => {
    const { dispatch, userData } = this.props;

    if (_.isEmpty(value.rangepicker)) {
      const value = userData.account
      dispatch({
        type: 'data/GET_HISTORICALRECORD_5',
        payload: value,
      })
    } else {
      value = { ...value.rangepicker, account: userData.account };
      dispatch({
        type: 'data/POST_HISTORICALRECORD_TIME',
        payload: value,
      })
    }

  }

  handleCompareCancel = (e) => {
    console.log(e);
    this.setState({
      Comparevisible: false,
    });
  }

  handleCompare = () => {
    this.setState({
      Comparevisible: true,
      CompareProgress: 50
    });

  }

  render() {
    const { emgData, userData, HistoricalRecord, LastEvaluation } = this.props;
    const loading_icon = <div style={{
      width: '100px',
      height: '100px',
      backgroundImage: 'url("https://i.imgur.com/t1q8b9P.gif")',
      backgroundSize: '100%'
    }} ></div>;
    return (
      <div>
        <Breadcrumb>
          <Breadcrumb.Item>數據管理</Breadcrumb.Item>
          <Breadcrumb.Item>個人狀況</Breadcrumb.Item>
        </Breadcrumb>
        <Row>
          {
            _.isNull(emgData) ?
              <div className={style.loading}>
                <Spin tip="Loading..." indicator={loading_icon} />
              </div> :
              <div>
                <Col lg={24}>
                  <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />最新一次的檢測紀錄</div>} extra={!_.isEmpty(emgData) ? (<Button onClick={this.handleCompare} style={{ backgroundColor: '#EDAE49', borderColor: '#EDAE49', color: '#ffffff' }}>狀態評估</Button>) : null} bordered={false}><CardDataMarker emgData={emgData}></CardDataMarker></Card>
                </Col>
                <Col lg={8}>
                  <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />RMS</div>} bordered={false}><CardBasicAnalysisLineChart container="rms" data={emgData}></CardBasicAnalysisLineChart></Card>
                </Col>
                <Col lg={8}>
                  <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />MF</div>} bordered={false}><CardBasicAnalysisLineChart container="mf" data={emgData}></CardBasicAnalysisLineChart></Card>
                </Col>
                <Col lg={8}>
                  <Card title={<div><Icon type="line-chart" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />MPF</div>} bordered={false}><CardBasicAnalysisLineChart container="mpf" data={emgData}></CardBasicAnalysisLineChart></Card>
                </Col>
              </div>

          }
        </Row>
        <Row gutter={40}>
          {
            _.isNull(HistoricalRecord) ?
              <div className={style.loading}>
                <Spin tip="Loading..." indicator={loading_icon} />
              </div> :
              <div>
                <Col className="gutter-row" lg={12} md={24}>
                  <Card title={<div><Icon type="book" theme="outlined" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />歷史紀錄</div>} bordered={false}>
                    <RangePickerForm handleChange={this.handleChange}></RangePickerForm>
                    <div>
                      <List
                        dataSource={HistoricalRecord}
                        bordered
                        renderItem={item => (
                          <List.Item key={item.id} actions={[<a onClick={() => this.showDrawer(item)}>詳細資料</a>]}>
                            <List.Item.Meta
                              avatar={
                                _.isNull(userData.img) ? <Avatar size="large" icon="user" /> : <Avatar size="large" src={`${apiConfig.api}/${userData.img}`} />
                              }
                              title={item.date}
                              description={`${item.isLR} - ${item.part}`}
                            />
                          </List.Item>
                        )}
                      />
                      {
                        _.isEmpty(HistoricalRecord) ? (<div> <Button size="large" target="_blank" href="https://drive.google.com/file/d/1SgepfgThZ5S5gpeLpTfSt9PDBb_TeZM9/view" style={{ display: 'block', margin: '0 auto' }}>您尚未進行復健，快來下載App!</Button> </div>) : null
                      }
                      {
                        _.isEmpty(this.state.record) ? null : (<ListDrawer record={this.state.record} visible={this.state.visible} onClose={this.onClose}></ListDrawer>)
                      }
                    </div>
                  </Card>
                </Col>
                <Col className="gutter-row" lg={12} md={24}>
                  {/*<Card title={<div><Icon type="area-chart" theme="outlined" style={{ verticalAlign: 'middle', fontSize: '25px', marginRight: '10px', color: 'rgb(23, 46, 66)' }} />頻域圖表 - 假資料 - 假資料</div>} bordered={false}><BasicArea></BasicArea></Card>
                   <div>
                {
                  _.isEmpty(this.state.record) ? null : (<ListDrawer record={this.state.record} visible={this.state.visible} onClose={this.onClose}></ListDrawer>)
                }
              </div> */}
                </Col>
              </div>
          }


        </Row>
        {
          _.isEmpty(emgData && LastEvaluation) ? <Modal
            title="狀態評估"
            visible={this.state.Comparevisible}
            onCancel={this.handleCompareCancel}
            className={style.compare}
            width='60%'
            footer=''
          >
            <Spin tip="Loading..." indicator={loading_icon} />
          </Modal> :
            <Modal
              title="狀態評估"
              visible={this.state.Comparevisible}
              onCancel={this.handleCompareCancel}
              className={style.compare}
              width='60%'
              footer=''
            >
              <div style={{ backgroundColor: '#ffffff', paddingTop: '50px', paddingBottom: '50px' }}>

                <div style={{ padding: '0 50px' }}>
                  <p style={{ fontSize: '22px', fontWeight: '700' }}>{emgData[0].isLR} - {emgData[0].place} - {emgData[0].part}</p>
                  <Row gutter={8} type="flex" justify="space-between" align="bottom">
                    <Col className="gutter-row" style={{ textAlign: 'right', paddingRight: '0' }} span={7}>
                      <Progress type="dashboard" percent={LastEvaluation[0].rms} format={percent => `${percent}%`} />
                      <h1>RMS</h1>
                    </Col>
                    <Col className="gutter-row" style={{ textAlign: 'center' }} span={10}>
                      {
                        (LastEvaluation[0].avg > 100) === true ?
                          <Progress type="dashboard" className={style.hundred} percent={LastEvaluation[0].avg} width={250} status="active" format={() => '正常'} />
                          :
                          (LastEvaluation[0].avg > 50) === true ?
                            <Progress type="dashboard" className={style.fifity} percent={LastEvaluation[0].avg} width={250} strokeColor="orange" status="active" format={percent => `${percent}%`} />
                            :
                            <Progress type="dashboard" className={style.zero} percent={LastEvaluation[0].avg} width={250} strokeColor="red" status="exception" format={percent => `${percent}%`} />
                      }

                      <h1>總分</h1>
                    </Col>
                    <Col className="gutter-row" style={{ textAlign: 'left', paddingLeft: '0' }} span={7}>
                      <Progress type="dashboard" percent={LastEvaluation[0].mf} format={percent => `${percent}%`} />
                      <h1>MF</h1>
                    </Col>
                  </Row>
                  <p style={{ fontSize: '22px', fontWeight: '700' }}>{emgData[1].isLR} - {emgData[1].place} - {emgData[1].part}</p>
                  <Row gutter={8} type="flex" justify="space-between" align="bottom">
                    <Col className="gutter-row" style={{ textAlign: 'right', paddingRight: '0' }} span={7}>
                      <Progress type="dashboard" percent={LastEvaluation[1].rms} format={percent => `${percent}%`} />
                      <h1>RMS</h1>
                    </Col>
                    <Col className="gutter-row" style={{ textAlign: 'center' }} span={10}>
                      {
                        (LastEvaluation[0].avg > 100) === true ?
                          <Progress type="dashboard" className={style.hundred} percent={LastEvaluation[1].avg} width={250} status="active" format={() => '正常'} />
                          :
                          (LastEvaluation[0].avg > 50) === true ?
                            <Progress type="dashboard" className={style.fifity} percent={LastEvaluation[1].avg} width={250} strokeColor='#f3e410' status="active" format={percent => `${percent}%`} />
                            :
                            <Progress type="dashboard" className={style.zero} percent={LastEvaluation[1].avg} width={250} strokeColor='#ed4949' status="exception" format={percent => `${percent}%`} />
                      }
                      <h1>總分</h1>
                    </Col>
                    <Col className="gutter-row" style={{ textAlign: 'left', paddingLeft: '0' }} span={7}>
                      <Progress type="dashboard" percent={LastEvaluation[1].mf} format={percent => `${percent}%`} />
                      <h1>MF</h1>
                    </Col>
                  </Row>
                  <Row gutter={8} type="flex" justify="space-between" align="bottom">
                    <div>
                      <h3>以總體分數來看，數值越接近0，肌肉萎縮程度越嚴重</h3>
                      <h3>數值越接近50，越有可能擁有肌肉萎縮</h3>

                    </div>

                  </Row>
                </div>
              </div>
            </Modal>
        }

      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    userData: _.get(state, 'auth.userData', null),
    emgData: _.get(state, 'data.dataMarker', null),
    HistoricalRecord: _.get(state, 'data.HistoricalRecord', null),
    LastEvaluation: _.get(state, 'data.LastEvaluation', null),
  }
}

export default connect(mapStateToProps)(Private);

import React, { Component } from 'react';
import { routerRedux } from 'dva/router';
import { Layout, Breadcrumb } from 'antd';
import style from './Profile.css';
import ProfileForm from '../../../component/form/Profile';
import { connect } from 'dva';
import _ from 'lodash';

const { Header } = Layout;

class Profile extends Component {

  state = {
  }

  componentDidMount() {

    const value = JSON.parse(localStorage.getItem('userInfo'));
    _.isEmpty(value) ? (this.props.dispatch(routerRedux.push('/member/notlogin'))) : this.setRenderComponent(this.props)

  }

  setRenderComponent() {
    const { dispatch } = this.props;
    dispatch({ type: 'auth/GET_USERDATA' })

    // _.isNull(localStorage.getItem('userInfo')) ? dispatch({ type: 'auth/GET_USERDATA' }) : console.log(userData)
  }

  handleEdit = (value) => {
    const { dispatch } = this.props;
    const callback = () => {
      console.log(this.state.isloading)
      this.setState({
        isloading: false,
      })
    }
    this.setState({
      isloading: true,
    }, () => {
      dispatch({
        type: 'auth/POST_EDITPROFILE',
        payload: value,
        callback,
      })
    })

  }

  render() {
    const { userData } = this.props;
    return (
      <div>
        <Breadcrumb>
          <Breadcrumb.Item>數據管理</Breadcrumb.Item>
          <Breadcrumb.Item>會員資料</Breadcrumb.Item>
        </Breadcrumb>
        <div className={style.p - 5} style={{ minHeight: '830px' }} >
          <Header style={{ background: '#fff', padding: 0, fontSize: '30px', minHeight: 100, marginTop: '10px', marginBottom: '-30px' }}>
            會員資料
        </Header>
          <ProfileForm userData={userData} handleEdit={this.handleEdit} isloading={this.state.isloading} ></ProfileForm>
        </div>
      </div>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    userData: _.get(state, 'auth.userData', null),
  }
}

export default connect(mapStateToProps)(Profile);


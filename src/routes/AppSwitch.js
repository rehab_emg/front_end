import React, { Component } from 'react';
import DashboardLayout from '../Layout/DashboardLayout';
import IndexLayout from '../Layout/IndexLayout';
import MemberLayout from '../Layout/MemberLayout';
import { Switch } from 'dva/router';

class AppSwitch extends Component {

  render() {
    const { children, location } = this.props;
    const { pathname } = location;

    if (pathname.indexOf('/dashboard') > -1) {
      return (<DashboardLayout><Switch>{children}</Switch></DashboardLayout>)
    }else if(pathname.indexOf('/member') > -1){
      return (<MemberLayout><Switch>{children}</Switch></MemberLayout>)
    }
     else if(pathname.indexOf('./' > -1)) {
      return (<IndexLayout><Switch>{children}</Switch></IndexLayout>)
    }
  }

}

export default AppSwitch;

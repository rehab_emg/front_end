import React, { Component } from 'react';
import { Layout, Menu, Row, Col, Form, Carousel, Card } from 'antd';
import style from './Index.css';
import { connect } from 'dva';
import CardSeriesChart from '../../component/card/chart/SeriesChart';
import FeaturesCard from '../../component/card/index';
const { Meta } = Card;

class Index extends Component {
  render() {
    return (
      <div>
        <div className={style.back} >
          <div>
            <div className={style.pic}>
              <Row className={style.title} >

                <img src="/src/assets/REHAB-LOGO.png" alt="" style={{ width: '170px', textAlign: 'center', display: 'block', margin: 'auto', padding: '15px 0' }} />
                <h1 style={{ textAlign: 'center', color: 'white', fontSize: '30px' }} >見肌行事</h1>
                <h2 style={{ textAlign: 'center' }}>肌肉萎縮評估與延緩之研究</h2>
              </Row>
            </div>
          </div>
        </div>
        <div style={{ width: '100%', maxWidth: 1200, margin: 'auto', textAlign: 'center' }}>
          <div className={style.mt}>
            <h1 style={{ textAlign: 'center', fontSize: '24px' }} >肌電圖裝置結合使用者應用程式</h1>
            <h1 style={{ textAlign: 'center', fontSize: '24px' }} >達到檢測肌電值的變化及個人化復健課程</h1>
          </div>
          {/* <Row gutter={24} className={style.mt}>
            <Col lg={14} md={24}>
              <FeaturesCard title="個人化復健環境" content="藉由收集個人基本的生理參數，經統計及資料分析後，推薦一個最" content2="適合自己的復健方式，提供使用者參考，並根據使用者過去的復健" content3="成效來判斷是否該加強復健的課程" ></FeaturesCard>
            </Col>
            <Col lg={10} md={24}>
              <img src="/src/assets/data.png" style={{ borderRadius: '10px', width: '100%', minHeight: 330 }} alt="" />
            </Col>
          </Row>
          <Row gutter={24} className={style.mt}>
            <Col lg={10} md={24} >
              <img src="/src/assets/doctor.jpg" style={{ borderRadius: '10px', width: '100%', minHeight: 330 }} alt="" />
            </Col>
            <Col lg={14} md={24}>
              <FeaturesCard title="減少醫療負擔與資源浪費" content="傳統的復健治療，須前往醫院的復健室且身旁須有治療師陪伴。透" content2="過本系統，可讓患者隨時隨地進行復健，透過即時、客觀的肌電判" content3="斷，做到只需定期回診，減少醫療負擔及資源的浪費" ></FeaturesCard>
            </Col>
          </Row>
          <Row gutter={24} className={style.mt}>
            <Col lg={14} md={24}>
              <FeaturesCard title="即時檢測與查看" content="透過肌電裝置的檢測，能將數據即時傳輸到行動裝置，透過系統的" content2="分析，了解當下的復健狀態，此外，使用者也可透過行動裝置查詢" content3="自身的復健狀態成效來判斷是否該加強復健的課程" ></FeaturesCard>
            </Col>
            <Col lg={10} md={24}>
              <img src="https://picsum.photos/506/300" style={{ borderRadius: '10px', width: '100%', minHeight: 330 }} alt="" />
            </Col>
          </Row>
          <Row gutter={24} className={style.mt}>
            <Col lg={10} md={24} >
              <img src="https://picsum.photos/506/300" style={{ borderRadius: '10px', width: '100%', minHeight: 330 }} alt="" />
            </Col>
            <Col lg={14} md={24}>
              <FeaturesCard title="隨時隨地皆可進行復健" content="本系統是使用Seeed Studio公司所生產的肌電圖檢測儀感測器，其" content2="大小適合使用者攜帶，且可在任何地方進行，讓使用者不會因時間" content3="或地點的不合適而減少復健的頻率" ></FeaturesCard>
            </Col>
          </Row> */}
        </div>
        <div style={{ width: '100%' }}>
          <Row className={style.mt}>
            <Col className={style.function_back} span={20} offset={2} style={{ 'background': 'none' }}>
              <Col span={8} className={style.function_card}>
                <img alt="function1" src="/src/assets/finction-new1.png" className={style.function_picture}></img>
                <p className={style.function_title}>檢測不便 </p>
                <Col span={19} offset={3}>
                  {/* <p className={style.function_text}>藉由收集個人基本的生理參數，經統計及資料分析後，推薦一個最適合自己的復健方式</p> */}
                  <p className={style.function_text}>傳統肌電檢測需自行前往醫院，但因距離、交通、門診時間等因素，導致每次想要檢測時都會花費許多力氣、時間與金錢成本。 </p>
                </Col>
              </Col>
              <Col span={8} className={style.function_card}>
                <img alt="function2" src="/src/assets/finction-new2.png" className={style.function_picture}></img>
                <p className={style.function_title}>傳統檢測報告閱讀不易 </p>
                <Col span={19} offset={3}>
                  <p className={style.function_text}>傳統檢測報告內容多是醫學術語、專有名詞，患者往往難以理解，在閱讀上有相當大的困難。 </p>
                  {/* <p className={style.function_text}>本研究利用輔助治療的方式，讓患者可以在家檢測自身情形並改善，解決了每次復健要前往醫院或診所的麻煩，也使醫療資源與成本增加的問題下降</p> */}
                </Col>
              </Col>
              <Col span={8} className={style.function_card}>
                <img alt="function3" src="/src/assets/finction-new3.png" className={style.function_picture}></img>
                <p className={style.function_title}>復健意願下降</p>
                <Col span={19} offset={3}>
                  <p className={style.function_text}>因復健過程乏味，加上無法從檢測結果清楚得知肌肉狀態，患者不易感受復健帶來的效果，使復健意願下降，導致產生挫敗感。</p>
                </Col>
              </Col>
              {/*<Col span={6} className={style.function_card}>
                <img alt="function4" src="/src/assets/function4.png" className={style.function_picture}></img>
                <p className={style.function_title}>隨時隨地進行檢測</p>
                <Col span={19} offset={3}>
                  <p className={style.function_text}>本研究使用的肌電圖檢測儀感測器，其大小適合攜帶，且檢測時不受門診時間而無法檢測，讓使用者不會因時間或地點的不合適而減少檢測的頻率</p>
                </Col>
              </Col>*/}
            </Col>
          </Row>
        </div>
        <div>
          <Row>
            <Col span={18} offset={3} style={{ marginTop: '10px', marginBottom: '100px ' }} >
              <Carousel className={style.carousel} effect="fade" afterChange={this.onChange} >
                <div>
                  <div>
                    <Col span={16} >
                      <CardSeriesChart></CardSeriesChart>
                    </Col>
                    <Col span={8} className={style.carouselcontent}>
                      <h2 style={{ color: 'black', marginBottom: '20px' }}>一般人和肌肉萎縮患者肌電比較圖</h2>
                      <h3 style={{ 'color': 'black', lineHeight: '40px' }}>白色線條為肌肉無萎縮的正常肌電圖</h3>
                      <h3 style={{ 'color': 'black' }}>紅色線條為肌肉萎縮患者的肌電圖</h3>
                    </Col>
                  </div>
                </div>
                <div>
                  <div>
                    <Row gutter={24} style={{ width: '100%', maxWidth: 1200, margin: 'auto', paddingTop: 20 }}>
                      <Col span={12} >
                        <Card
                          hoverable
                          // style={{ width: '50%' }}
                          cover={<img alt="example" src="https://picsum.photos/506/240" />}
                        >
                          <Meta
                            title="醫咡叅祀五六七八"
                            description="醫咡叅祀五六七八醫咡叅祀五六七八"
                          />
                        </Card>
                      </Col>
                      <Col span={12} >
                        <Card
                          hoverable
                          // style={{ width: '50%' }}
                          cover={<img alt="example" src="https://picsum.photos/506/240" />}
                        >
                          <Meta
                            title="醫咡叅祀五六七八"
                            description="醫咡叅祀五六七八醫咡叅祀五六七八"
                          />
                        </Card>
                      </Col>
                    </Row>
                  </div>
                </div>
              </Carousel>
            </Col>
          </Row>
        </div>
        <div>
          <Row className={style.mt}>
            <Col className={style.app} span={18} offset={3}>
              <img alt="app_phone" src="/src/assets/app_phone.png" className={style.app_phone}></img>
              <img alt="triangle" src="/src/assets/triangle.png" className={style.back_triangle}></img>

              <div className={style.app_cover}>
                <div style={{ display: 'block' }}>
                  <h1 className={style.app_title} >APP操作流程</h1>
                </div>
                <Row>
                  <Col xl={16} lg={17} offset={8}><p style={{ lineHeight: '35px', marginBottom: '25px' }} className={style.step_text} ><img alt="app_step" src="/src/assets/step1.png" className={style.step}></img>登入成功後於左方功能列點選檢測肌電，選擇檢測部位後進入檢測頁面</p></Col>
                  <Col xl={16} lg={17} offset={8}><p style={{ lineHeight: '50px', marginBottom: '43px' }} className={style.step_text} ><img alt="app_step" src="/src/assets/step2.png" className={style.step}></img>開啟肌電感測器，將貼片黏貼至檢測部位指定位置</p></Col>
                  <Col xl={16} lg={17} offset={8}><p style={{ lineHeight: '35px', marginBottom: '25px' }} className={style.step_text} ><img alt="app_step" src="/src/assets/step3.png" className={style.step}></img>於APP檢測頁面連接感測器，連接完畢即可開始檢測，檢測過程於APP畫面顯示肌電數據</p></Col>
                  <Col xl={16} lg={17} offset={8}><p style={{ lineHeight: '50px' }} className={style.step_text} ><img alt="app_step" src="/src/assets/step4.png" className={style.step}></img>結束時點擊停止檢測按鈕，並將檢測數據上傳至雲端</p></Col>
                </Row>
              </div>
            </Col>
          </Row>
        </div>
        <div>
          <Row className={style.mt}>
            <Col className={style.info} span={18} offset={3}>
              <div className={style.cover}>
                <h1 style={{ 'fontWeight': '600', 'fontSize': '3em' }} >打造不受時間與空間限制的檢測環境</h1>
                <h2>透過開發肌電評估行動應用程式(Application,APP)的方式來進行對肌電圖的判定</h2>
                <h2>讓患者不經常性的到醫院及診所進行復健治療&nbsp;&nbsp;&nbsp;&nbsp;也可以透過本身的狀況及醫生或復健師的建議</h2>
                <h2>設定個人化的復健方案&nbsp;&nbsp;&nbsp;&nbsp;讓使用者隨隨地進行檢測</h2>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default connect()(Index);

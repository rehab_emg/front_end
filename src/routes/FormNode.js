import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import _ from 'lodash';
import { injectIntl } from 'react-intl';
import { Button, Card, Table } from 'antd';

const loginForm = [{
  label: '電子信箱',
  key: 'email',
  message: '您輸入的不是電子信箱!',
  icon: 'mail',
  type: 'input',
  required: [true, '請輸入電子信箱']
}, {
  label: '密碼',
  key: 'password',
  icon: 'lock',
  type: 'input',
  required: [true, '請輸入密碼']
}]

const registerForm = [{
  label: '電子信箱',
  key: 'email',
  message: '您輸入的不是電子信箱!',
  icon: 'mail',
  type: 'input',
  required: true,
}, {
  label: '密碼',
  key: 'password',
  icon: 'lock',
  type: 'input',
  required: [true, '請輸入密碼']
}, {
  label: '密碼確認',
  key: 'passwordcheck',
  icon: 'lock',
  type: 'input',
  required: [true, '請輸入密碼確認']
}, {
  label: '姓名',
  key: 'name',
  icon: 'user',
  type: 'input',
  required: [true, '請輸入姓名']
}, {
  label: '生日',
  key: 'birth',
  type: 'DatePicker',
  required: [true, '請輸入生日']
}, {
  label: '電話',
  key: 'phone',
  icon: 'phone',
  type: 'input',
  required: [true, '請輸入電話']
}]

class FormNode extends Component {

}
export default connect()(FormNode);

import React, { Component } from 'react';
import { Modal } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import ResetForm from '../../../component/form/ResetPassword';

class ResetPassword extends Component {

  state={
    restVisible: true
  }

  handleCancel =() =>{
    this.setState={
      restVisible: false
    }
  }

  handleResetSubmit = (value, callback) =>{
    const { dispatch } = this.props;

    dispatch({
      type: 'auth/POST_RESET',
      payload: value,
      callback,
      close: this.handleCancel(),
    })
  }

  // componentDidMount() {
  // }

  render() {
    return (
      <Modal
        visible={this.state.restVisible}
        onCancel={this.handleCancel}
        footer={null}
      >
        <ResetForm handleResetSubmit={this.handleResetSubmit}></ResetForm>
      </Modal>

    )
  }
}

export default connect()(ResetPassword);

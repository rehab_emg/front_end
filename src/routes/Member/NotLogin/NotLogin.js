import React, { Component } from 'react';
import { Modal } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

class NotLogin extends Component {
  error() {
    Modal.error({
      title: '未登入帳號',
      content: '請先登入帳號',
    });
    setTimeout(() => this.props.dispatch(routerRedux.push('/')), 100);
  }

  componentDidMount() {
    this.error();
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}
export default connect()(NotLogin);

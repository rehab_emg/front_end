import React, { Component } from 'react';
import { Modal } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

class Success extends Component {

  success = () => {
    const modal = Modal.success({
      title: '恭喜您驗證成功',
      content: '現在可以登入!',
    });
    setTimeout(() => this.props.dispatch(routerRedux.push('/')), 100);
  }

  componentDidMount() {
    this.success();
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}

export default connect()(Success);

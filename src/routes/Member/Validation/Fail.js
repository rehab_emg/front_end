import React, { Component } from 'react';
import { Modal } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

class Fail extends Component {

  error() {
    Modal.error({
      title: '驗證失敗',
      content: '驗證碼輸入錯誤',
    });
    setTimeout(() => this.props.dispatch(routerRedux.push('/')), 100);
  }

  componentDidMount() {
    this.error();
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}

export default connect()(Fail);

import React, { Component } from 'react';
import { Modal } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

class RegisterSuccess extends Component {

  success = () => {
    const modal = Modal.success({
      title: '恭喜您註冊成功',
      content: '接下來請去信箱接收驗證信',
    });
    setTimeout(() => this.props.dispatch(routerRedux.push('/')), 100);
  }

  componentDidMount() {
    this.success();
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}

export default connect()(RegisterSuccess);

import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import Dashboard from './routes/Dashboard/Dashboard';
import AppSwitch from './routes/AppSwitch';
import Profile from './routes/Dashboard/Profile/Profile';
import Public from './routes/Dashboard/Public/Public';
import Private from './routes/Dashboard/Private/Private';
import ChangePassword from './routes/Dashboard/ChangePassword/ChangePassword';
import RegisterSuccess from './routes/Member/RegisterSuccess/RegisterSuccess';
import ValidateSuccess from './routes/Member/Validation/Success';
import ValidateFail from './routes/Member/Validation/Fail';
import Resetpassword from './routes/Member/ResetPassword/ResetPassword';
import NotLogin from './routes/Member/NotLogin/NotLogin';
import Index from './routes/Index/Index';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <AppSwitch>
          <Route path="/" exact component={Index} />
          <Route path="/dashboard" exact component={Dashboard} />
          <Route path="/dashboard/profile" exact component={Profile} />
          <Route path="/dashboard/public" exact component={Public} />
          <Route path="/dashboard/private" exact component={Private} />
          <Route path="/dashboard/changepassword" exact component={ChangePassword} />
          <Route path="/member/success" exact component={RegisterSuccess} />
          <Route path="/member/validation/success" exact component={ValidateSuccess} />
          <Route path="/member/validation/fail" exact component={ValidateFail} />
          <Route path="/member/resetpassword" exact component={Resetpassword} />
          <Route path="/member/notlogin" exact component={NotLogin} />
        </AppSwitch>

      </Switch>
    </Router>
  );
}

export default RouterConfig;

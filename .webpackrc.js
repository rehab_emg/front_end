import path from 'path';

export default {
  "entry": "src/index.js",
  "publicPath": "/",
  "extraBabelPlugins": [
    "transform-decorators-legacy",
    "transform-class-properties",
    ["import", { "libraryName": "antd", "style": true }],
  ],
  env: {
    development: {
      extraBabelPlugins: ['dva-hmr'],
    },
  },
  "alias": {
    "assets": path.resolve(__dirname, 'src/assets'),
  }
}

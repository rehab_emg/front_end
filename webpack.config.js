import Dotenv from 'dotenv-webpack';


export default function(webpackConfig) {
  webpackConfig.plugins.push(new Dotenv());
  return webpackConfig;
}
